# Implementación del algoritmo de neuroevolución por topologías aumentadas para el control de un sistema caótico

## Algoritmos Genéticos
Los algoritmos genéticos inspirados en el concepto de la selección natural de Darwin para la discriminación de soluciones de una función dada. De forma general, el objetivo de utilizar de un algoritmo genético es encontrar una solución óptima a un problema, definiendo una función de aptitud con la cual serán evaluadas las diversas soluciones. Para encontrar la solución óptima se itera a través de múltiples generaciones que estarán consutuidas por varios "individuos", mismos que hacen referencia a la evaluación de la función que se quiere optimizar con distintos parámetros. Los individuos de la generación son evaluados en base a la función de aptitud, las soluciones con mejor desempeño serán seleccionadas para combinarse y mutarse para producir soluciones hijo, que a su vez volverán a ser evaluadas en un proceso iterativo hasta encontrar la mejor solución posible.

#### Historia
Los algoritmos genéticos fueron inventados por John Holland en los años 60s y fueron desarrollados por Holland y sus estudiantes y colegas en la universidad de Michigan entre los años 1960s y 1970s. En contraste con las estrategias de evolución y la programación evolutiva, la meta principal de Holland no era diseñar un algoritmo para resolver problemas específicos, sino estudiar formalmente el fenómeno de adaptación como sucede en la naturaleza y así desarrollar formas en las que los mecanismos de adaptación natural puedan ser representados en sistemas computacionales. Holland presentó los algoritmos genéticos en su libro sobre los sistemas naturales y artificiales como una abstracción de la evolución biológica y dio un marco teórico para la adaptación.

#### Operadores de los Algoritmos Genéticos
La forma más simple de algoritmos genéticos involuran tres tipos de operadores: selección, combinación y mutación.  
**Selección**: Este operador selecciona el cromosoma en la población para la reproducción. Entre más apto sea el chromosoma, es más probable que sea seleccionado más veces para su reproducción.  
**Combinación**: Este operador selecciona de manera aleatoria un lugar e intercambia subsecuencias antes y después del lugar entre dos cromosomas para crear dos hijos. Por ejemplo, las cadenas de caracteres 10000100 y 11111111 podrías ser combinadas después del lugar en cada uno para producir los hijos 10011111 y 11100100. El operador de combinación imita de manera muy general la combinación entre dos organismos de cromosomas simples (haploide).  
**Mutación**: Este operador modifica de manera aleatoria algún bit en el chromosoma con el objetivo de asegurar la diversidad de individuos en la generación. Por ejemplo, la cadena 00000100 podría ser mutada en su segunda posición para producir 01000100. La mutación puede ocurrir en cada posición de bit según una probabilidad definida, normalmente muy reducida (ej. 0.001). 


#### Implementación
1. Empezar con una población generada aleatoriamente de n cromosomas de l-bits (soluciones candidato)
2. Calcular la aptidud o adaptabilidad 



#### Evolución 
1. Para una generación, toma una porción de los individuos que hayan mostrado mejo desempeño calculado en base a la función de desempeño o función de aptitud. Esos individuos más "aptos" serán padres de las siguientes generaciones. También selecciona aleatoriamente



#### Limitaciones ??
- La evaluación repetida de la función de adaptación (fitness function) para problemas complejos es regularmente el segmento más prohibitivo y limitante de los algoritmos evolutivos. Encontrar la solución óptima a problemas multi-dimensionales complejos requiere evaluaciónes de la función de apdaptación muy costosas en términos de recursos computacionales. En problemas del mundo real como problemas de optimización estructural, una sola evaluación de la función puede requerir varias horas o días para terminar la simulació. Los métodos usuales de optimización  no pueden manejar ese tipo de problemas, En este caso, puede ser necesario realizar una evaluación no exacta y usar una aproximación de la adaptabilidad que es computacionalmente eficiente. 
- 




## Redes Neuronales Artificiales

Las redes neuronales artificiales son en términos generales, una abstracción matemática que pretende modelar el funcionamiento básico de nuestro cerebro. El ter

#### Tipos de redes neuronales
- Feedforward NN: La información únicamente se mueve en una dirección de input a output. El tiempo no tiene afectación alguna en el algoritmo y no existen ciclos o iteraciones.
- Recurrent NN: Redes con flow de datos bi-direccional. A diferencia de las redes neuronales feedforward que propagan la linealmente la información de input a output, las redes neuronales recurrentes también propagan información de las etapas posteriores de procesamiento a las etapas anteriores.
- Modular NN



#### Métodos de entrenamiento
- Reducción del error
- Backpropagation
- 

#### Diseño de topología de una red neuronal








## NEAT

Neuroevolución por topologías aumentadas

#### Termiología

Gen:                    Cada individuo o red consta de dos tipos de genes: genes de nodos y genes de conexiones
Genoma:                 Individual
Fenotipo:               Representación "real" de un genotipo - Network
Pool de genes:          Population
Marcadores históricos:  Permiten saber el orígen de un gen y ayuda a resolver el problema de convenciones competidoras


Número de innovación:  
Especie:            Conjunto de genomes que tienen la misma topología   

#### Generalidades

El objetivo principal del algoritmo de Neuroevolución por Topologías Aumentadas (NEAT) es desarrollar un algoritmo genético que busque la mejor solución de una red neuronal modificando tanto sus pesos como su topología y tiene las siguientes propiedades:
1. Existe una representación genética de la red que permite que la combinación de las estructuras sea de una manera significativa
2. Protege la innovación topológica que necesita evolucionar algunas generaciones para ser optimizada y no así desaparezca del pool de geneomas prematuramente
3. Minimizar topologías mediante entrenamiento y funciones de penalización


#### Codificación de los genotipos (genomas)
La representación del genotipo tiene una codificación directa, esto quiere decir que el número de nodos, conexiones y pesos de un genotipo está extresado de manera literal en la representación. Por ejemplo el encoding de la red neuronal solución al problema de OR exclusivo con el mínimo número de neuronas es:

Inputs: Todos son numerados con entedos negativos, desde -1 hasta -inf. No son considerados dentro de los nodos de la clase de genotipo porque no tienen sesgo ni activación

![xor](./thesis-images/neat/neat_net_xor.png)

```python
Genome(
    nodes=[
        { "key" : 0, "bias" : b0, "activation" : "sigmoid"}         # Output
        { "key" : 1, "bias" : b1, "activation" : "sigmoid"}         # Hidden neuron
    ],
    links=[
        { "key" : (-2,0), "weight" : w0, "enabled" : True }
        { "key" : (-2,1), "weight" : w1, "enabled" : True }
        { "key" : (-1,0), "weight" : w2, "enabled" : True }
        { "key" : (-1,1), "weight" : w3, "enabled" : True }
        { "key" : (1,0), "weight" : w4, "enabled" : True }
    ]
)
```

**Genes de Nodos**
{ key=0, bias=-1.43, resp=1.0, activation=sigmoid }   -   Neurona de salida
{ key=1, bias=-2.3, resp=1.0, activation=sigmoid }    -   Neurona de la capa oculta
(representa dos neuronas con sesgo de -1.43 y -2.3, ambas con función de activación sigmoidal)

**Genes de Conexiones*
{ key=(-2,0), weight=2.5, enabled=True }              -   Neurona de input B a salida
{ key=(-2,1), weight=-2.31, enabled=True }              -   Neurona de input A a salida
{ key=(-1,0), weight=-2.67, enabled=True }              -   Neurona de input B a salida
{ key=(-1,1), weight=4.04, enabled=True }              -   Neurona de input B a salida
{ key=(1,0), weight=6.155, enabled=True }              -   Neurona de input B a salida


#### Convenciones competidoras 
Convenciones competidoras significa tener más de una manera para expresar una misma solución a un proplema de optimización de pesos con una red neuronal. Cuando los genomas representando la misma solución no tienen el mismo encoding, el cruzamiento puede producir descendientes o "hijos" defectuosos. El principal indeicador en NEAT es que el origen histórico de dos genes es evidencia directa de la homología si los genes comparten el mismo origen. Por lo tanto, NEAT realiza sinápsis artificiales basadas en marcadores históricos, permitiendo cambiar la topología de la red sin perder la pista de cada gen en el curso de una simulación.
- Más de una manera de expresar una misma solución de optimización de pesos
- NEAT lo resuelve colocando marcadores históricos a los genes, permitiendo cambiar la estructura de la red sin perder la pista de los genes a lo largo de la simulación

#### Proteger la innovación con especiación
Frecuentemente, agregar nodos en una red hace que inicialmente baje su desempeño antes de permitir que sus pesos se optimicen. Por ejemplo aumentar la estructura introduce no linearidades en donde lo las había antes. Es improbable que un nuevo nodo o conexión expresen una buena respuesta tan pronto como son introducidos a la red. Desafortunadamente, como inicialmente hay una pérdida de desempeño del genoma, la innovación es improbable que sobreviva en una población que ya se ha optimizado previamente. Por tal motivo, es necesario  proteger de alguna manera las innovaciones estructurales de la red para que puedan hacer uso y optimizar esa  nueva estructura.
En la naturaleza, diferentes estructuras tienden a pertenecer a diferentes grupos o especies que comiten en diferentes nichos. Por lo tanto, innovación es implícitamente protegida dentro del nicho. De manera similar, si las redes con estructuras novedosas pudieran estar aisladas en su propia especie, tendrían oportunidad de optimizar sus estructuras antes de tener que competir con el grueso de población.
La especiación requiere de una función de compatibilidad que indique si dos genomas deberían estar en la misma especie o no. Es difícil formular esta función de compatibilidad entre redes de distintas topologías. 
Como NEAT guarda información histórica del gen, la población en NEAT puede ser fácilmente separado en especies, Se usa la división explícita de desempeño (explicit fitness sharing), la cual forza individuos con genomas similares a compartir su desempeño. 


#### Población inicial 
En NEAT, a diferencia de TWEANNs, inicia la población con pocas neuronas y las cuales están interconectadas entre sí, sin capas de neuronas oculatas.
Una manera de minimizar las redes, es incorporando el tamaño de la red a la función de desempeño. En tales casos, el desempeño será penalizado para las redes más grandes.
La similaridad puede ser fácilmente medida basado en información histórica en los genes.
Una alternativa para no tener que modificar la función de desempeño, es que el mismo método de neuroevolución tienda a la minimalidad. Si la población empieza sin nodos ocultos y la estructura crece sólo conforme beneficie a la solución, no hay necesidad de modificar la función de desempeño. Por lo tanto, empezar con una población minimalista e ir creciendo su estructura es un principio de diseño en NEAT.


#### Mutaciones
**Pesos**
[Mutación no estructural]

**Nodos**
[Mutación estructural]
Siempre que se agrega un nodo en la red, se hace añadiéndolo entre dos nodos conectados, asignando peso de 1 para que no haya tanta afectación al sistema. Por lo tanto cuando se hace una mutación de nodo se agregan 3 genes: un gen de nodo y dos genes de conexión. Tomemos como ejemplo una red simple a la que se le quiere agregar un nuevo nodo. Los genes quedarían de la siguiente manera:

Ejemplo de agregar un nodo entre Nodo0 (output) y NodoC (input):
- Se deshabilita el link entre los nodos 0  y C
- Se agrega el nuevo nodo (Nodo2)
- Se agrega gen de conexión entre NodoC y Nodo2 con peso de 1 
- Se agrega gen de conexión entre Nodo2 y Nodo0 con el peso que tiene el gen deshabilitado entre Nodo0 y NodoC   

**Antes de la mutación**  
![node_mutate1](./thesis-images/neat/neat_mutate_node_1.png)
```python
Genome(
    nodes=[
        { "key" : 0, "bias" : b0, "activation" : "sigmoid"},    # Output
        { "key" : 1, "bias" : b1, "activation" : "sigmoid"}     # Hidden
    ],
    links=[
        { "key" : (-3,0), "weight" : w0, "enabled" : True  },   
        { "key" : (-2,1), "weight" : w1, "enabled" : True  },  
        { "key" : (-1,1), "weight" : w2, "enabled" : True  },
        { "key" : (-1,0), "weight" : w3, "enabled" : True  },
        { "key" : (1,0), "weight" : w4, "enabled" : True  },
    ]
)
```
**Después de la mutación**  
![node_mutate2](./thesis-images/neat/neat_mutate_node_2.png)
```python
Genome(
    nodes=[
        { "key" : 0, "bias" : b0, "activation" : "sigmoid"},    # Output
        { "key" : 2, "bias" : b1, "activation" : "sigmoid"}     # Hidden
        { "key" : 2, "bias" : b2, "activation" : "sigmoid"}     # New Hidden
    ],
    links=[
        { "key" : (-3,0), "weight" : w0, "enabled" : False  },  # Deshabilitado
        { "key" : (-2,1), "weight" : w1, "enabled" : True  },   
        { "key" : (-1,1), "weight" : w2, "enabled" : True  },
        { "key" : (-1,0), "weight" : w3, "enabled" : True  },
        { "key" : (1,0), "weight" : w4, "enabled" : True  },
        { "key" : (-3,2), "weight" : 1, "enabled" : True  },    # Peso de 1 innov+1
        { "key" : (2,0), "weight" : w0, "enabled" : True  }     # Peso de C a 0 innov+2
    ]
)
```


**Conexiones**
[Mutación estructural]


#### Monitoreo de genes por medio demarcadores históricos
- Dos genes con el mismo origen histórico deben representar la misma estructura (aunque posiblemente con pesos distintos), pues los dos son derivados del mismo gen ancestro de algún punto en el pasado. Por lo tanto la necesidad de alinear los genes entre ellos es para seguir la pista del origen histórico de cada gen del sistema.
Trackear el origen histórico requiere muy poca computación. Cuando un nuevo gen aparece (por medio de mutación estructural), un __global innovation number___ se incrementa y se asigna a ese **gen**. El innovation number por lo tanto representa una cronología de las apariciones de cada gen en el sistema

* Cuando se agrega un nuevo gen al genoma por mutación estructural (node/link mutation), se incrementa un global innovation number y se le asigna al nuevo gen creado. Esto quiere decir que todos los genes.
Un problema que se puede presentar es que la misma innovación estructural reciba diferentes __innovation numbers__ en la misma generación

Para hacer la recombinación entre los genomas, éstos de alinean conforme el innovation number de los genes y considerando únicamente los genes de las conexiones. Por ejemplo si un genotipo tiene un innovation number y otro no, se considera un __disjoint__ (desarticulación) si está dentro del rango del innovation number de ambos, si no está dentro del rango del innovation number de alguno, éste se considera un __excess__ (exceso).
Cuando se conforma el genoma hijo, los genes son seleccionados aleatoriamente de cualquier padre en los genes alineados, y los genes desarticulados o en exceso siempre son incluidos del padre más apto (con mejor desempeño). 

* Los genes que están alineados se heredan de manera aleatoria
* Los genes no alineados (desarticulados o en exceso) son heredados del padre más apto
(todos). Si el desempeño de ambos es igual se realiza de manera aleatoria

Ejemplo de cruzamiento entre genomas
```python
# Padre 1
parent1 = Genome(
    nodes=[
        { "key" : 0, "bias" : b0, "activation" : "sigmoid"},    
        { "key" : 1, "bias" : b1, "activation" : "sigmoid"}    
    ],
    links=[
        { "key" : (-3,0), "weight" : w10, "enabled" : True  },   
        { "key" : (-2,1), "weight" : w11, "enabled" : True  },  
        { "key" : (-1,1), "weight" : w12, "enabled" : True  },
        { "key" : (-1,0), "weight" : w13, "enabled" : True  },
        { "key" : (1,0), "weight" : w14, "enabled" : True  },
    ]
)
# Padre 2
parent2 = Genome(
    nodes=[
        { "key" : 0, "bias" : b0, "activation" : "sigmoid"},    
        { "key" : 1, "bias" : b1, "activation" : "sigmoid"},
        { "key" : 2, "bias" : b2, "activation" : "sigmoid"}    
    ],
    links=[
        { "key" : (-3,1), "weight" : w20, "enabled" : True  },  
        { "key" : (-3,0), "weight" : w21, "enabled" : True  },   
        { "key" : (-2,1), "weight" : w22, "enabled" : True  },  
        { "key" : (-1,2), "weight" : w23, "enabled" : True  },  
        { "key" : (-1,0), "weight" : w24, "enabled" : True  },  
        { "key" : (1,2), "weight" : w25, "enabled" : True  },
        { "key" : (2,3), "weight" : w26, "enabled" : True  }
    ]
)
``` 

**Hijo**
```python
# Padre 2
parent2 = Genome(
    nodes=[
        { "key" : 0, "bias" : b0, "activation" : "sigmoid"},        # Random
        { "key" : 1, "bias" : b1, "activation" : "sigmoid"},        # Random
        { "key" : 2, "bias" : b2, "activation" : "sigmoid"}         # Random
    ],
    links=[
        { "key" : (-3,1), "weight" : w20, "enabled" : True  },      # Parent 2
        { "key" : (-3,0), "weight" : w20, "enabled" : True  },      # Parent 2
        { "key" : (-2,1), "weight" : w20, "enabled" : True  },      # Parent 1
        { "key" : (-1,2), "weight" : w20, "enabled" : True  },      # Parent 2
        { "key" : (-1,1), "weight" : w20, "enabled" : True  },      # Parent 1
        { "key" : (-1,0), "weight" : w20, "enabled" : True  },      # Parent 1
        { "key" : (1,2), "weight" : w20, "enabled" : True  },       # Parent 2
        { "key" : (2,0), "weight" : w20, "enabled" : True  }        # Parent 2
    ]
)
```

#### Protección de la innovación mediante especiación
Crear especies dentro de la población permite que los organismos compitan primordialmente con los de su mismo nicho en vez de competir con el grueso de la población. La idea es dividir la població en especies, de tal forma que las topologías similares queden en la misma especie. Parecería que esta acción corresponde enteramente a un pareo de topologías, sin embargo los marcadores históricos proveen una solución efficiente al problema. Entre más desarticulados sean dos genomas, menos historia de evolución comparten y por lo tanto son menos compatibles entre sí. Podemos medir la distancia de compatibilidad d de distintas estructuras como una simple combinación lineal del número de genes de excesos y desarticulaciones , así como el promedio de la diferencia entre pesos de los genes pareados (alineados), incluyendo los genes deshabilitados.
```python
d = c1*(E/N) + c2*(D/N) + c3*avg(W)
```
d : distancia de compatibilidad
E : número de genes de exceso
D : número de genes desarticulados
W : pesos de la red
ci : coeficientes para ajustar la importancia de los tres factores
N : número de genes en el genoma más grande 

La distancia de compatibilidad nos permite comparar contra un umbral de compatibilidad definido por nosotros. En cada generación, los genomas son sequencialmente incluidos en las especies. 
Cada especie tiene un organismo representante que es seleccionado de manera aletoria de la generación anterior. 
Un genoma g en la generación en curso es colocado en la primer especie con la cual sea compatible, midiendo la distancia entre él y cada representante de esa especie. De esta manera las especies no se traslapan. Si g no es compatible con ningún representante de las especies, una nueva especie es creada
Fitness sharing: 
```python
adjusted_fitness_j = fitness_j / sum( sh(d(i,j)), range(i,n) )
# Otra forma de expresarlo
adjusted_fitness_j = fitness_j / num_organisms_same_species_of_j
```
j   : número de genoma específico
i   : número total de neuronas
d   : función que regresa la distancia de compatibilidad entre i y j
sh  : sharing function  (regresa 1 o 0)

**Sharing function**
Si la distancia entre el genoma i y j es mayor que el umbral de compatibilidad, la función regresa 0. Si la distancia es menor que el umbral, la función regresa 1. 
* Por ejemplo si hay una especie con un sólo individuo la función de sharing sería:
adjusted_fitness = fitness / 1
* Por ejemplo si hay una especie con 3 individuos, se divide entre 3

Las especies entonces se reproducen primero eliminando los genomas menos aptos de la población. La población entera es reemplazada por los hijos de ls organismos reminentes de cada especie


Importante:
* El número de excesos y desarticulaciones entre un par de genomas es una medida natural para medir su compatibilidad. 
* Se toma un representante aleatorio de la especie de la generación previa
* Para colocar a un organismo en una especie se mide la distancia vs el representante de cada especie, si es compatible se asigna a esa especie
* Si el genoma a especiar no es compatible con ningún representante, se crea una nueva especie
* El fitness sharing entre individuos de la misma especie tiene como propósito penalizar a las especies con muchos individos, así una sola especie no toma todo el control
* Fitness sharing function: fitness = fitness / num_organismos_de_la_especie
* Toda la especiación se reduce a ajustar el fitness considerando el número de individuos parecidos



#### Parámetros usados para prueba XOR del paper 
* Población de 150 individuos
* Los coeficientes para medir compatibilidad:
  * c1 = 1.0
  * c2 = 1.0
  * c3 = 0.4
* dt = 3.0 (compatibility distance threshold)
* El campeón de cada especie con más de 5 individuos sería copiado íntegro a la siguiente generación
* 80% prob de mutación de los pesos
  * Cada peso tiene 90% de ser modificado uniformemente
  * 10% de que se le asigne un nuevo valor aleatorio
* 75% chance de heredar gen desahibitado si estuviere deshab en cualquier padre
* 25% de los hijos resulta de mutaciones sin cruzamiento
* Pareo de interespecies de 0.001
* 0.03 de probabilidad de agregar nuevo nodo
* 0.05 probabilidad de mutación por link


### Reverse engineering de neat python con XOR











#### Configuración de parámetros

 


(Explicación breve del funcionamiento del péndulo invertido)



## Péndulo Caótico
#### Modelado


## Manos al Código...


## Bibliografía

- M. Tim Jones, AI Application Programming
- NN Design
- Melanie Mitchell, An introduction to Genetic Algorithms, page 
- Evolving weights in a fixed network / Intro to genetic algorithms. p50 




#### Resources

https://lethain.com/genetic-algorithms-cool-name-damn-simple/

http://gekkoquant.com/2016/03/13/evolving-neural-networks-through-augmenting-topologies-part-1-of-4/

Reinforcement learning in autonomous agents
http://repositorio.ipl.pt/bitstream/10400.21/1144/1/Disserta%C3%A7%C3%A3o%20Ingl%C3%AAs 