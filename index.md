# Índice general
## Comparación de control PID vs control evolutivo para el estabilizar un péndulo invertido

* Introducción
* Objetivos
* Antecedentes
  * Algoritmos Genéticos
  * Redes Neuronales
  * Neuroevolución
    * Evolución de pesos
    * Evolución de topología
* Estado del Arte 
  * Tipos de control
    * PID
    * State Variables
    * Fuzzy
    * Non linear
    * Control inteligente
* Planteamiento del problema
  * Péndulo invertido y control con PID
  * Péndulo invertido doble y control con PID
  * Control clásico (PID)
* Reinforcement learning 
  * Behaviour evaluation
* Neuroevolución
  * Competing conventions problem
  * Topological innovation
* NEAT
  * Generalidades
  * Esquema de codificación y marcadores históricos
    * Crossover
    * Especiación
    * Mutaciones 
* Implementación
* Resultados
* Conclusiones


Intro 
Objetivos

Cap 1. Antecedentes de la Neuroevolución
- Redes Neuronales
  - Activaciones
- Métodos de entrenamiento y optimización
- Algoritmos genéticos


Cap 2. Antecedentes de Control
- Modelado de sistemas físicos 
  - Variables de estado
  - Lagrange
- Control PID
- Control por variables de estado
- Control no lineal


Cap 3. Planteamiento del problema
- Modelo péndulo
- Modelo péndulo doble


Cap 4. Péndulo simple
- Control PID
- Control NEAT 


Cap 5. Péndulo doble
- Control PID
- Control NEAT


Cap 5. Manos al código
- Algoritmo genético
- Red Neuronal
- Péndulo simple
- Péndulo doble
- NEAT 




--------------------------------------------------------

Cap1 - Antecedentes
- Cálculo
- Algebra lineal
- Optimizaciones


Cap2 - Redes Neuronales
- Perceptrón
- Tipos de redes
- Activación
- Función de coste (error)
- Entrenamiento

Cap3 - Algoritmos Genéticos
- Historia
- Descripción del algoritmo
- Neuroevolución

Cap4 - Neuroevolución
- Evolución de pesos en una red fija
- Evolución de arquitectura de la red
- Codificación directa e indirecta
- NEAT
  - Competing conventions problem
  - Topological innovation

Cap5 - Modelo y Control
- Modelo péndulo invertido
- Modelo péndulo invertido doble
- Control PID péndulo invertido
- Control Neuroevolutivo

Cap6 - Manos al Código
- Arquitectura del paquete
- Modelos y clases
- Implementación
- Uso
- Documentación

Cap7 - Resultados

Cap8 - Conclusiones



--------------------------------------------


Cap 1 Introducción
  Abstract
  Motivación
  Justificación
  Objetvos

Cap 1 Antecedentes
  Sistemas dinámicos
    [Definición]
    [Tipos]
    Modelado
      [Herramientas matemáticas]
    Linealidad
    Estabilidad
    Caos
  Control
    [Tipos de control]
  Inteligencia Artificial
    Historia y evolución
  Algoritmos genéticos
    Conceptos generales
    Operadores genéticos
    Implementación
  Redes Neuronales
    [generalidades, historia, tipos de redes, tipo de aprendizaje]
    Conceptos generales
      [neurona, capa, peso...]
    Aprendizaje supervisado
      [definición]
    Aprendizaje no supervisado
      [definición]
    Aprendizaje reforzado
      [definición]
  Neuroevolución
    [generalidades y definiciones]



Cap 1 NEAT
  Visión general
  Encoding
  Mutación 
    Pesos
    Enlaces
    Topología
  Reproducción
  Especiación 
  Parámetros de configuración
  

Cap 2 Modelo y Control
  Diagrama de Cuerpo Libre
  Modelado del sistema
  Análisis de estabilidad
  Control neuroevolutivo


Cap 3 Diseño de Neuropy
  Visión general
  Requerimientos
  Tecnología
  Proceso principal del sistema
  Entidades
  Paquetes
  Arquitectura
  

Cap 4 Manos al código
  Metodología de desarrollo (Incremental)
  Requerimientos técnicos
  Instalación de Python en Linux/OSX
  Ambiente de desarrollo
  Módulos y clases
  Estructura de archivos
  Ejecución


Cap 5 Simulaciones y Resultados
  Función XOR
  Péndulo simple
  Péndulo doble


Cap 6 Conclusiones