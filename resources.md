# Resources

neat-python     https://github.com/CodeReclaimers/neat-python
pears           https://github.com/noio/peas


# Double pendulum
double-pendulum     http://blog.otoro.net/2015/02/11/cne-algorithm-to-train-self-balancing-double-inverted-pendulum/
modeling and simulation https://en.wikipedia.org/wiki/Double_pendulum
double pendulum and chaos   https://12d82b32-a-62cb3a1a-s-sites.googlegroups.com/site/physicistatlarge/Computational%20Physics%20Sample%20Project-Alex%20Small-v1.pdf?attachauth=ANoY7cqfUmR-0EvO6Kfo_y2Dyuz7l2ZyYcK8OKmCTRkElq88J2l85LKlW1rjcqVyGNuDmBzEBJqnMr3QMf2vI3kKAuiOKfDwJ-qxmoYqeNEU_q1IpzEbhz11n21XjtrIIHLOvkMjp1_HwrWiaV_Vd0-JR2n17zCMC3xKPaG_NLaYU1GUVku8jnwRodMvFFc5bbYgyP1Y02frcOyCqs0mOTPVuNBH1zw0vFmQz4pNtpRsgzz-fDWFg97H3oVrQVO6T5ZOs95mqZlrUeJ5FOStU3XkMy0Oe2gd9Q%3D%3D&attredirects=0

Animation with matplotlib   https://jakevdp.github.io/blog/2012/08/18/matplotlib-animation-tutorial/
Backpropagation             http://neuralnetworksanddeeplearning.com/chap2.html
Backpropagation video       https://www.youtube.com/watch?v=gl3lfL-g5mA
Evolving weights            https://medium.com/@harvitronix/lets-evolve-a-neural-network-with-a-genetic-algorithm-code-included-8809bece164
Kearas (NN Library)          https://keras.io/

Reinforcement Learning Pkg  https://gym.openai.com/



# Sistemas dinámicos