#-*- coding: utf-8 -*-



''' Given the function:  f(y) = y+|sin(32y)| ; 0< =y <pi
    Fix the values that maximize the function
    candidate solutions are all the possible values of y
    that can be encoded to represent a bit string 
    The fitness calculation translates a given bit string x into a real number y and then evaluates the function at that value
'''