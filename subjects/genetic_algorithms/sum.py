#-*- coding: utf-8 -*-
from random import randint, random
from operator import add
from functools import reduce
import math

''' Genetic algorithm to find the solutions to 
    Find a list of N numbers that summed together give X
    Individuals of the end population should ve the most fit
'''


def individual(length, min, max):
    ''' Create a member of the population
    '''
    return [ randint(min,max) for x in range(length) ]


def population(count, length, min, max):
    ''' Create a population of *count* individuals
        count:      the number of individuals in the population 
        length:     the number of values per individual
        min:        the min value that can be in an individial
        max:        the max value in an individual
    '''
    return [ individual(length, min, max) for x in range(count) ]


def fitness(individual, target):
    ''' Determine the fitness of an individual, the lower the better
        individual:     individual to evaluate
        sum:            the target value of the summed values of the ind
    '''
    sum = reduce(add, individual, 0)
    return abs(target-sum)


def grade(pop, target):
    ''' Evaluate an entire population's fitness 
    '''
    summed = reduce(add, (fitness(x,target) for x in pop), 0)
    return summed/(len(pop)*1.0)


def evolve(pop, taget, retain=0.2, random_select=0.05, mutate=0.01):
    ''' Evolution
        1. Get the top (retain)% best performing individuals
        2. Randomly get the (random_select)% of less performing individuals
        3. Insert a mutation in (mutate)% individuals 
        4. Breeding the rest of the population with crossover of the parents
    '''
    # Get the best performing individuals
    graded = [ (fitness(x, target),x) for x in pop ]
    graded = [ x[1] for x in sorted(graded) ]
    retain_length = int(len(graded)*retain)
    parents = graded[:retain_length]

    # Randomly add other individuals to promote diversity
    for individual in graded[retain_length:]:
        if random_select > random():
            parents.append(individual)
    
    # Mutate individuals
    for individual in parents:
        if mutate > random():
            # Get random position of the value to mutate
            pos_to_mutate = randint(0,len(individual)-1)
            # Mutate the value (not ideal min, max)
            individual[pos_to_mutate] = randint(min(individual),max(individual))

    # Crossover parents
    parents_length = len(parents)
    desired_length = len(pop) - parents_length
    children = []
    while len(children) < desired_length:
        male_i = randint(0,parents_length-1)
        female_i = randint(0,parents_length-1)
        if male_i != female_i:
            male=parents[male_i]
            female=parents[female_i]
            half = int(len(male)/2)
            #half_male = math.ceil(half)
            #half_female = math.floor(half)
            child = male[:half] + female[half:]
            children.append(child)

    parents.extend(children)
    return parents

    
if __name__ == '__main__':
    target = 317
    p_count = 100
    i_length = 5
    i_min = 0
    i_max = 100
    p = population(p_count, i_length, i_min, i_max)
    fitness_history = [grade(p,target),]
    for i in range(1000):
        p = evolve(p, target)
        fitness_history.append(grade(p,target))

    print("Last population of solutions: "+str(p))
    
    for datum in fitness_history:
        print(datum)
