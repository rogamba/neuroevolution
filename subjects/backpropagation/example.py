import numpy as np

''' Ejemplo de la creación de una red neuronal entrenada con 
    retropropagación dada una función de pruebas
    Enter an initial net configuration
'''

LEARNING_RATE=0.5
HIDDEN_LAYER=3
INPUT_LAYER=2
OUTPUT_LAYER=1


def surface(x1,x2):
    ''' z = x1^2 + 2x1x2 + 2x2^2 + 1
    '''
    return x1**2 + 2*x1*x2 + 2*x2**2 + 1




def gen_training_set():
    ''' Returns a key value tuple of input:output
        ( [[x1],[x2]] , z )
    '''
    training_domain=[]
    for x1 in np.arange(-2,2,0.2):
        for x2 in np.arange(-2,2,0.2):
            training_domain.append( (x1,x2) )
    training_range = [ ( np.matrix([x1,x2]).transpose() , round(surface(x1,x2),1) ) for (x1,x2) in training_domain ]
    return training_range


def sigmoid(x=None,deriv=False):
    ''' Sigmoid function (used for hidden neurons)
    '''
    if deriv == True:
        return x*(1-x)
    return 1/(1+np.exp(-x))


def purelin(x=None,deriv=False):
    ''' Linear (usually used in output neurons)
    '''
    if deriv = True:
        return 1
    return vector


# Vectorize the functions to accept vectors and return vectors
vsigmoid = np.vectorize(sigmoid)
vpurelin = np.vectorize(purelin)



# Initial random weights
weights_layer1 = np.matrix( np.random.randn(hidden_layer_size, input_layer_size) )
weights_layer2 = np.matrix( np.random.randn(output_layer_size,hidden_layer_size) )
bias_layer1 = np.matrix( np.random.randn(HIDDEN_KLAYER,1) )
bias_layer2 = np.matrix( np.random.randn(OUTPUT_LAYER,1) )

# For x1 = 1, x2 = 1 : z = 6
x1 = 1
x2 = 1
a0 = np.matrix([x1,x2]).transpose()
p_vector = np.matrix([x1,x2]).transpose()


# Hacemos el feedforward
z1 = np.dot(weights_layer1.transpose(),p_vector) + bias_layer1
a1 = sigmoid(z1)
z2 = np.dot( weights_layer3.transpose(), a1) + bias_layer2
a2 = purelin(z2)


# calculamos el error (t - a)
cost = np.matrix( [surface(x1,x2) - a2] ).transpose() * np.matrix( [surface(x1,x2) - a2] )

# Target value
t = surface(x1,x2)

# Variation odf Cost func with respect to the inputs
dl2 = np.multiply( (a2 - t) , sigmoid(z2,deriv=True))
dCdW2 = np.dot(a1,dl2.transpose()) 
dCdb2 = dl2

dl1 = np.multiply( np.dot(weights_layer2,dl2) , sigmoid(z1,deriv=True) )
dCdW1 = np.dot(a0,dl2.transpose()) 
dCdb1 = dl1



# Obtenemos el error de la última a la primers
dl2 = np.multiply( np.dot(weights_layer2,dCdz) , sigmoid(z1,deriv=True) )
dCdW2 = np.dot(a1,dl2.transpose()) 
dCdb2 = dl2
# Obtenemos el error de la capa previa
dl1 = np.multiply( np.dot(weights_layer2,dCdz) , sigmoid() )


class NeuralNet:

    weights = None
    layers = None

    def __init__(inputs=None,ouputs=None,hidden=[]):
        # Generate the vectors and matrices given the chars of the net
        self.vsigmoid=np.vectorize(sigmoid)
        self.vpurelin=np.vectorize(purelin)
        pass

    
    def sigmoid(self,x=None,deriv=False):
        ''' Sigmoid function (used for hidden neurons)
        '''
        if deriv == True:
            return x*(1-x)
        return 1/(1+np.exp(-x))

    def purelin(self,x=None,deriv=False):
        ''' Linear (usually used in output neurons)
        '''
        if deriv = True:
            return 1
        return vector


    def feed_forward():
        ''' Loop every layer to get the expected output
        '''
        for layer in layers:
            ln = self.tranfer_function(  )
        pass


    def error_propagation():
        pass


    def adjust_weights():
        pass
    





def sigmoid(x=None,deriv=False):
    ''' Sigmoid function (used for hidden neurons)
    '''
    if deriv == True:
        return x*(1-x)
    return 1/(1+np.exp(-x))