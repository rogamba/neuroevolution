import numpy as np
import sys
import random

''' Ejemplo de la creación de una red neuronal entrenada con 
    retropropagación dada una función de pruebas
    Enter an initial net configuration
'''

LEARNING_RATE=0.5
HIDDEN_LAYER=2
INPUT_LAYER=1
OUTPUT_LAYER=1


# Compute the delta weights and bias
'''
    Wm(k+1) = Wm(k) - alpha * delta_Wm
    bm(k+1) = bm(k) - alpha * delta_bm
'''

# Calculating sensitivities
'''
    In term of the sensitivities
    Wm(k+1) = Wm(k) - alpha * Sm * (a_m1)^T
    bm(k+1) = bm(k) - bm(k) * alpha * Sm
    Where:
    Sm = dF/dn = [ dF/dnm_1, dF/dnm_2, ..., dF/dnm_sm ]

    Computando la variación de los inputs de la capa m+1 con respecto de la capa m 
    La matriz jacobiana quedaría: (asumiendo una red 2:3:2)

    dn2/dn1 = [[ dn2_1/dn1_1 , dn2_1/dn1_2, dn2_1/dn1_3],
                [ dn2_2/dn1_1 , dn2_2/dn1_2, dn2_2/dn1_3]]

    analizando dn2_1/dn1_1...

    n2_1 (inputs de la neurona 1 de la segunda capa) = ( w2_11*a1_1 + w2_21*a1_2 + w2_31*a1_3 ) + b2_1
    w2_11 : peso segunda capa de neurona 1 (capa1) a neurona 1 (capa2)
    a1_1  : activación (output) de la neurona 1 capa 1 ( a = f(n) )

    Por lo tanto la parcial con respecto del input de la neurona 1 de la capa anterior es:

    dn2_1/dn1_1 = w2_11 * d/dn1_1(a1_1) = w2_11 * f1'(n1_1)

    Y por lo tanto la matriz jacobiana puede traducirse en forma matricial en:

    dn2/dn1 = W2 * F1'(n1) 

    donde:

    F1'(n1) =  [[f1'(n1)     0        ],
                [0           f1'(n2)],]

    Por lo tanto la sensibilidad:
    Sm = Fm'(nm) * (Wm+1)^T * Sm+1 


    Para obtener la última sensibilidad (de donde empezar el backpropagation...)
    Sm = -2*Fm'(nm) * (t-a) 

    Ajstando pesos con la sensibilidad
    W(k+1) = W(k) + alpha * S^m (a^m-1)^T
    b(k+1) = b(k) + alpha * S^m 


    Batch training
    Iteramos por todos los puntos del set de entrenamiento e incrementamos los pesos conforme al promedio del error


'''


# INCREMENTAL TRAINING
# Obtenemos training set
# Obtenemos un punto aleatorio del training set
# Hacemos feedforward, obtenemos la función de error
# Obtenemos la sensibilidad de la última capa
# Obtenemos el resto de las sensibilidades
# Ajustamos pesos de cada capa


# BATCH TRAINING
# Obtenemos el training set
# While error > x...
#   Iteramos por cada punto del training set
#       Hacemos el forward pass
#       Obtenemos la sensibilidad de la última capa
#       Obtenemos el resto de las sensibilidades
#   Ajustamos pesos




def regfunc(x):
    y = 1 + np.sin( (np.pi/4)*x )
    return y


def gen_training_set():
    ''' Returns a key value tuple of input:output
        ( [[x1],[x2]] , z )
    '''
    training_domain=[]
    for x1 in np.arange(-2,2,0.2):
        training_domain.append( (x1) )
    training_set = [ ( np.matrix([x1]).transpose() , round(regfunc(x1),4) ) for (x1) in training_domain ]
    return training_set


def sigmoid(x=None,deriv=False):
    ''' Sigmoid function (used for hidden neurons)
    '''
    if deriv == True:
        return sigmoid(x)*(1-sigmoid(x))
    return 1/(1+np.exp(-x))


def purelin(x=None,deriv=False):
    ''' Linear (usually used in output neurons)
    '''
    if deriv == True:
        return 1 
    return x 


# Vectorize the functions to accept vectors and return vectors
sigmoid = np.vectorize(sigmoid)
purelin = np.vectorize(purelin)



class NeuralNet:

    layers=None
    trainig_set=[]              # Training set
    training_batch = []         # Batch of net params to adjust weights and bias
    W=[]                        # List of weights
    b=[]
    f=[]
    a=[]
    n=[]
    s=[]
    p=[]
    t=[]
    mse=100

    def __init__(self,layers=2,W=W,b=b,f=f,strategy="incremental"):
        self.layers = layers        # int of number of layers
        self.W = W                  # list of weight matrices by layer
        self.b = b                  # list of bias vectors by layer
        self.f = f                  # list of functions by layer
        self.F = f                  # list of matrices of function derivatives valuated
        self.eta = 0.2              # Learning Rate
        self.strategy=strategy      # Training strategy (bathch or incremental)
        self.training_set = gen_training_set()
        self.training_batch = [None]*len(self.training_set)
        print(self.training_set)
        pass    


    def feedforward(self):
        self.a = []
        self.n = []
        # Iterate layers
        for i in range(0,self.layers):
            # a^m-1 = p if i = 0
            act = self.pk if len(self.a) <= 0 else self.a[i-1]
            # Get input vector of layer
            self.n.append( np.dot(self.W[i],act) + self.b[i] )
            # Apply activation to input 
            self.a.append( f[i](self.n[-1]) )


    def differential_matrices(self,):
        self.F = []
        for m in range(0,self.layers):
            # get vector input in m layer
            n_prime = self.f[m](self.n[m],deriv=True)   
            # Construimos matriz diagonal del vector
            Fm = np.diag(n_prime.A1)
            # hacemos el append a la lista
            self.F.append(Fm)




    def sensitivities(self):
        # Generate F'(n)
        self.differential_matrices()
        # Get last one
        self.s = [ None for i in range(0,self.layers) ]
        # sM = -2 * FM * (t-a)
        self.s[-1] = np.dot( -2*self.F[-1] , (self.tk - self.a[-1]) )
        # Propagate sensitivity backwards
        for m in range(self.layers-2,-1,-1):
            self.s[m] = np.dot( np.dot( self.F[m] , self.W[m+1].transpose()) , self.s[m+1] )



    def save_batch(self):
        #print("Saving batch: "+str(q))
        self.training_batch.append({
            "W" : self.W,
            "s" : self.s,
            "a" : self.a,
            "b" : self.b,
            "t" : self.tk,
            "p" : self.pk
        })


    def adjust_batch_weights(self):
        # Adjust weight layer by layer 
        for m in range(self.layers-1,-1,-1):
            sum_w = 0
            sum_b = 0
            for batch in self.training_batch:
                sum_w = sum_w + np.dot( batch['s'][m] , batch['a'][m-1].transpose() )
                sum_b = sum_b + batch['s'][m]
            # Adjusting the weights and bias
            self.W[m] = self.W[m] - (self.eta/len(self.training_batch)) * sum_w
            self.b[m] = self.b[m] - (self.eta/len(self.training_batch)) * sum_b


    def mean_square_error(self):
        sum_error=0
        for batch in self.training_batch:
            error = (batch["t"]-batch["a"][-1])
            sum_error = sum_error + np.dot(error.transpose(),error)
        self.mse = (1/len(self.training_batch)) * sum_error



    def adjust_weights(self):
        ''' Adjust weights for incremental training
        '''
        for m in range(self.layers-1,-1,-1):
            self.W[m] = self.W[m] - self.eta * np.dot( self.s[m] , self.a[m-1].transpose() )
            self.b[m] = self.b[m] - self.eta * self.s[m]



    def train(self):
        # Hacemos el pass
        for i in range(0,200):
            # Iteramos por los puntos del training set {p:input, t:target_output}
            self.training_batch=[]
            if self.strategy == 'incremental':
                random.shuffle(self.training_set)
            for p,t in self.training_set:
                
                # inputs and outputs
                self.tk = t
                self.pk = p
       
                # Hacemos el forward pass
                self.feedforward()

                # Test error
                self.error = self.tk - self.a[-1]  

                # Hacemos el backpropagation
                self.sensitivities()
                
                # Strategy
                if self.strategy == 'batch':
                    # If incremental trainig adjust weights
                    self.save_batch()
                else:
                    self.adjust_weights()

            print("ERROR: "+str(self.error))

            if self.strategy == 'batch':
                # Mean square error
                self.mean_square_error()
                # Ajustamos los pesos y bias de todo el training set
                self.adjust_batch_weights()
                
        

    def test(self,p=None):
        print("################# Test")
        self.pk = p
        self.feedforward()
        target = regfunc( p )
        print("Expected value: ", target)
        print("Output value: ",self.a[-1])




# Initial random weights
W1 = np.matrix([-0.27,-0.41]).transpose()
W2 = np.matrix([0.09,-0.17])
b1 = np.matrix([-0.48,-0.13]).transpose()
b2 = np.matrix([0.48])


# Valores iniciales por capas
layers = 2
W = [W1,W2]             # Lista de matrices de pesos
b = [b1,b2]             # Lista de vectores de bias
f = [sigmoid,purelin]   # Lista de funciones por capa

print("Backpropagation Example")
print("Initial values:")
print("W:")
print(W)
print("b:")
print(b)
print("Functions:")
print(f)


if __name__=='__main__':
    layers = 2
    W = [W1,W2]             # Lista de matrices de pesos
    b = [b1,b2]             # Lista de vectores de bias
    f = [sigmoid,purelin]   # Lista de funciones por capa
    net = NeuralNet(layers=layers,W=W,b=b,f=f,strategy='batch')
    net.train()
    net.test(p=np.matrix([3]).transpose())