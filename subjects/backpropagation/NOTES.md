# Backpropagation Algorithm 

## Antecedentes
- Linear algebra
- Partial derivatives
- Chain rule
- Perceptron learning rule
- Minimum square error
- Steepest descent

## History
La regla de aprendizaje del perceptrón fue desarollada por Frank Rosenblatt y el algoritmo LMS (Least Mean Square) de Bernard Widrow y Marcian Hoff fueron diseñados para entrnar una red de una sola capa de tipo perceptron. Estas redes de una sola capa tenían la desventaja de poder resolver únicamente problemas linalmente separables de clasificación. Pero ambos, Rosenblatt y Widrow estaban conscientes de estas limitaciones y propusieron redes multicapa que pudieran superarlas, pero no pudieron generalizar sus algoritmos para entrenar estas redes más complicadas.

No fue hasta mediados de los 80s que el algoritmo de retropropagación fue redescubierto y ampliamente difundido. Fue redescubierto independientemente por David Rumerhart, Geoffrey Hinton y Ronald Williams, David Parker y Yann Le Cun.

Proceso

- Al algoritmo se le tiene que alimentar con un set de ejemplos o de entrenamiento del comportamiento esperado de la red:   {p1,t1}, {p2,t2}, ... {pq,tq} (p - input, t - target value)
- El algoritmo debe ajustar los parámetros de la red para minimizar el mean square error: F(x) = E[(e**t)*(e)] = E[(t-a)**t * (t-a)]  donde x es el vector de pesos y sesgos de la red
- Si la red tiene múltiples outputs, se generaliza: F(x) = ( t(f) - a(k) )^t * ( t(k) - a(k) ) = e^t(k) * e(k)



Regresión de una función de ejemplo: x1**2 + 2*x1*x2 + 2*x2**2 + 1
- Propuesta de una red para obtener set de entrenamiento para 