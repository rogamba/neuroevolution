from evolution import Evolution
from phenotype import Phenotype
from double_pendulum import DoublePendulumCart
import config
import visualize

''' Steps to use the library:
    1. Set the configuration parameters
    2. Import the configuration variables
    3. Set the fitness function you would like to eval your genomes with
    4. Pass the config dict to the evolution instance
    5. RUn the evolution process passing the fitness function
        
'''

CONFIG_FILE = config.PATH+'config.json'

np.set_printoptions(suppress=True)
gamma = 0.99
beta = 0.00001
alpha = 0.000001
sigma = 0.001
w = np.array([0, 0, 0, 0, 0, 0, 0, 0])
delta_w = np.array([0,0,0,0,0,0,0,0])
v = np.array([0,0,0,0,0,0,0,0])
delta_v = np.array([0,0,0,0,0,0,0,0])


# Define fitness evaluation function for each genome
def eval_genome(genome):
    ''' Receives a list of genome objects and sets 
        the fitness to every genome
    '''
    # Create phenotype from the genome
    phenotype = Phenotype(config.params, genome)
    net = phenotype.create()
    cart = DoublePendulumCart()

    # How to calculate the fitness of the genome
    # Evaluate the genome for the maximum time


    # Fitness = avg time of all the episodes
    # How many episodes for the same net?
    
    s = cart.reset()
    a = get_action(s, v)
    times = []
    for e in range(5):
        steps = 0
        for n in rage(1,1000):
            # Render
            cart.render()
            s_, r, done, info = cart.step(a)

            # How many steps did the cart hold?
            step += 1
            action = net.activate(state)
            print(action)

            # update v (actor): ??
            delta_v = alpha*(((a - mu(s, v))*phi_actor(s)))*q_hat(s, a, w)
            v = np.add(v, delta_v)
 
            # update w (critic): ??
            delta_w = (beta*(r + gamma*q_hat(s_, a_, w) - q_hat(s, a, w)))*phi_critic(s, a)
            w = np.add(w, delta_w)

            s = s_
            a = a_

            if done or n >= 100:
                print("Episode finished after " + str(t+1) + " timesteps")
                times.append(step)
                break

    # Get the genome fitness (avg steps) * 10 = seconds
    fitness = np.mean(times) * 5
    return fitness
    



def run():
    print("[run] Starting main")
    # Get configuration parameters
    config.build(CONFIG_FILE)

    # Init the evolution object
    evolution = Evolution(config.params)

    # Run evolution
    winner = evolution.run(fitness_function=eval_genome,generations=config.params['generations'])
    
    # Show performance of winner vs actual value
    #evaluate_winner

    print('\nOutput:')
    ph = Phenotype(config.params, winner)
    winner_net = ph.create()
    for xi, xo in zip(xor_inputs, xor_outputs):
        output = winner_net.activate(xi)
        print("input {!r}, expected output {!r}, got {!r}".format(xi, xo, output))

    # Print the net
    node_names = {-1:'A', -2: 'B', 0:'A XOR B'}
    visualize.draw_net(config.params, winner, True, node_names=node_names, filename="plots/winner.gv")



if __name__ == '__main__':
    run()