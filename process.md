# General development process

### Tasks


[Math, GA & NN]
- Linear Algebra                                                            [done]                
- Optimization Methods                                                      [done]
- Neural Networks                                                           [done]
- Ejemplo gradient descent                                                  [done]
- Ejemplo newton                                                            [done]
- Terminar de aprender backpropagation                                      [done]
- Desarrollo de perceptrón multicapa con matrices                           [done]
- Ejemplo backpropagation (batch & incremental training                     [done]
- Leer y estudiar todo el artículo de Stanley                               [done]
- Examinar ejemplo de XOR de neat-python                                    [done]
- Crear nueva clase simplificada de neat-python                             [done]
- Módulo GA para evolucionar pesos de la red                                [discarded]
- Ejemplo función para encontrar solución con la clase de NN y GA           [discarded]
- Módulo GA para evolucionar topología (NEAT)                               [done]
- Crear/conseguir animación del double pedndulum on cart                    [done]


[Modeling & control]
- Estudiar paper de modelado de double inverted pendulum on cart            [doing]
- Estudiar paper de reinforcement learning de gustaffson                    [done]
- Estudiar clase de cartpole y rendering para animación                     [done]
- Integrar modelo y animación con NEAT                                      [done]
- Modificar modelo para balancear los 2 ángulos a 0 grados                  [doing]
- Obtención y gráfica de resultados                                         [pending]
  - Poner el resultado cuando NO se restringe el ángulo theta2 (se equilibria con el eslabón 2 ya volteado/caído)
  - Poner resultado restringiendo el ángulo theta2, se debería de equilibrar como esperado


[Thesis]
- Redacción de tesis y recopilación de fuentes                              [inProcess]




