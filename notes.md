# Implementación del algoritmo NEAT para balancear un péndulo invertido

Objetivos



## Introducción

El algoritmo NEAT (por sus siglas en inglés Neuro Evolution of Augmented Topologies)

Recientemente la evolución de las Redes Neuronales Artificiales (ANNs) han captado mcha atención de científicos alrededor del mundo.

En épocas recientes hemos visto un creciente interés en el área de inteligencia artificial y aprendizaje de máquina. Hoy en día podemos encontrar aplicación de estas tecnologías en prácticamente tofosk byestr



# Genetic Algorithms


## Antecedentes
- Algebra lineal (matrices, multiplicación, tranpuesta, etc...)
  - Matrices
  - Matrix multiplication
  - Transpose
  - Inverse
- Partial derivatives
- Logistic regression
- Perceptron
- Feedforward learning
- Backbropagation
- Genetic algorithms
- NEAT Applications
- Modelado de un péndulo invertido
- Aplicación de neat para el control de un péndulo invertido













## History
- GAs were developed by John Holland and his students and collegues at the university of Michigan in the 1960s and 1970s.

## GA Operators

The simplest form of genetic algorithms involves three types of operators: selection, crossover and mutation.  
**Selection**: This operator selects the chromosomes in the population for reproduction. The fitter the chromosome, the more times it is likely to be selected for reproduction.    
**Crossover**: This operator randomlychooses a locus and exchanfes the subsequences before and after that locus between two chromosomes to create two offspring. For example, the strin 10000100 and 11111111 could be crossover after the third locus in each to produce the two offspring 10011111 and 11100100. The crossover operator orughly mimics biological recombination between two single-chomosome (haploid) organism.    
**Mutation**: This operator randomly flips some of the bits in a chromosome. For example, the string 00000100 might be mutated in its seconf position to yield 01000100. Mutation can occur at each bit position in a string with some probability, usually very small (e.g., 0.001)


## Simple Genetic Algorithm
1. Start with randomply generated population of n l-bit chromosomes (candidate solutions to a problem)
2. Calculate the fitness of each chromosome x in the population
3. Repeat the following until n offsprings have been calculated:
  a. Selecton of a pair of chromosomes, the probability of selection is a function of theis fitness. The same chromosome can be selected more than once to become a parent.
  b. With probability pc (crossover probability), crossover the pair at a randomly chosen point. If no crossover takes place, form two offsprings that are exact copies of their respectice parents. 
  c. Mutate the two offsprings at each locus with the probability pm (mutation probability), and place the resulting chromosome in the new population
4. Replace the current population with the new population
5. Go to step 2
 

### 1. Defining a problem to optimize
- Define the target value of the function you want to get
- Define the individuals
- Define the number of individuals in a population

### 2. Evolution
1. For a generation, take a portion of the best performing individuals as judged by the fitness function. These high-performers will be parents of the next generation. Also randomly select some lesser performing individuals to be parents, because we want to promote genetic diversity so we don't get stuck in a local maximum, including some individuals who are not performing as well, will reduce our probability of getting stuck.
2. Breed together parents to the population to its desired size. 
3. Merge together the parents and children to constitute the next generation's population
4. Mutate a small random portion of the populatio. What this means is to have a probability of randomly modifying each individual. This, like taking poor performing individuals is for promoting diversity and prevent getting stuck in a local maxima


## Other techniques

- Produce more the fitter individuals produce more offsprings than the less fit ones


## Evolving Neural Networks
(Intro to neural networks)
Las redes neuronales son una aproximación al aprendizaje de máquina motivada en la neurociencia. Recientemente muchos esfuerzos se han hecho en utilizar algoritmos genéticos para evolucionar aspectos de las redes neuronales.


### Evolving weights in a fixed network
David Montana and Lawrence Davi (1989) took the first approach. evolving the weights in a fixed neural network. They were using GA instead of back-propagation as a way of finding a good set of weights for a fixed set of connections. Several problems associated with back-propagation algorithm (e.g. getting stuck in a local optima in weight space, or the unavailability of a "teacher" to supervise learning in som tasks) often make it desirable to find alternatice weithttraining schemes.  
Montana and Davis were interested in using neural networks to clasify underwater sonic "lofargrams" in: "interesting" and "not interesting".  
The net: 4 inputs units -> 7 units -> 10 units / +18 threshold  connections = 126 weights to evolve.   
Each chromosome was a vector of 126 weights. Encoding: The weights were rad off the network in a fixed order (from leg to right and from top to bottom) and placed in a list. Notice that each "gene" in the chromosome is a real number rather than a bit. To calculate the fitness of a given chromosome, the weights in the chromosome were assigned to the links in the corresponding network, the network was run on the training set (here 236 examples from the database of lofargrams), and the sum of the sware errors (collected over all the training cycles) was returned. Here, an "error" was the difference between the desired output activation value and the actual output activation value. Low error meant high fitness 
(La fitness se calculaba corriendo el training set y comparando los valores del output con los del chromosome)
An initial population of 50 weight vectors was chosen randomly, with each weight being between .0 and +1. Montana and David tried a number of different genetic operators in various experiments. 
- The mutation operator selects n noninput units and, for each incoming link to those units, adds a rondom value between .0 and +1 to the weight on the link. 
- The crossover operator takes two parent weight vectors and, for each non-input unit in the offspring vector, selects one of the parents at random and copies the weights on the incoming links from that parent to the offspring. Notice that only one offspring is created.

Comparing GA with backpropagation
(poner resultados y gráfica - Intro to genetic algorithms p.53)


### Evolving network architecture
Neural network researchers know all too well that the particular architecture chosen can determine the success or failure of the application, so they would like much to be able to automatically optimize the procedure of designing the architecture for a particular application. Many of the attempts may fall into two categories: direct encoding adn grammatical encoding. Under direct encoding, a network architecture is directly encoded into GA chromosome. Under grammatical encoding, the GA does not evolve network architectures: rather it evolves grammars that can be used to develop network architectures.

#### Direct Encoding
The direct encoding method is illustrated in work done by Geoffrey Miller, Peter Todd, and Shilesh Hedge (1989), who restricted theid initial project to feedforward with fixed number of units for which the GA was to evolve the connection topology. (example figure) **Connections that were specified to be learnable were initialized with small random weights**. Since Miller, Todd, adn Hegde restricted these networks to be feedforward, any connections to input units of feedbacj connections specified in the chromosome were ignored. 

  from unit      1   2   3   4   5
  to unit     1  0   0   0   0   0
              2  0   0   0   0   0
              3  L   L   0   0   0
              4  L   L   0   0   0
              5  0   0   L   L   0   
      (example of a direct encoding matrix)

#### Grammatical encoding
The method of grammatical encoding can be illustrated in the work of Hiroaki Kitano (1990), who points out that direct-encoding approachs become increasingly



### NEAT Algorithm
The NEAT Algorithm aims to develop a genetic algorithm that searching through neural network weith and structure space that has the following properties:
1. Have a genetic representation that allows structure to be crossed over in a meaningful way.
2. Protect topological innovation that need a few evolutions or "generations" to be optimised so that it doesn't disappear from the gene pool prematurely.
3. Minimise topologies throughout training whithout specially contrived network complexity penalisation functios

The information about the network is represented by a genome, the genome contains node genes and connection genes. The node genes define nodes in the network, the nodes con be inputs (such as technical indicator), outputs (such as a buy/sell recommendation), or hidden (used by network for calculation). The connection genes join nodes in the network together and have a weight attached to them.

Connection genes have an input node, an output node a weight, an enable/disable flag and an innovation number. The innovation number is used to track the history of a genes evolution.

Each genome has embedded inside it a mutation rate for each type of mutation that can occur, These mutation rates are aldo randomly increased or decreased as the evoltion progresses.

**Point mutate**    
Randomly updates the weight of a randomly selected connection gene
```
new_weight = old_weight +/- rand_num(between 0 and genome$MutationRate[["Step"]])
```
or
```
new_weight = rand_num between -2 and 2
```

**Link Mutate**  
Randomly adds a new connection to the network with a random weight between -2 and 2

**Node Mutate**  
This mutation adds a new node to the network by disablinga a connection, replacing it with a connection weight of 1, a node and a conenction with the same weight as the disabled connection. In essence it's been replaceed eith an identically functioning equivalent.  

**Enable Disable Mutate**  
Randombly enables and disables connections.  

#### Tracking gene history through innovation number
Each time a new gene is created (through a topological innovation) a global innovation number is incremented and assigned to that gene.
The global innovation number is tracking the historical  origin of each gene. If two genes have the same innovation number, then they must represent the same topology (although weights may be different). This is exploited during the gene crossover.

#### Genome Crossover (Mating)
Genomes crowwover takes two parent genomes (e.g. A and B) and creates a parent genome (child) taking the stronges genes from A and from B copying any topological structures along the way.  
During the crossover genes from both genomes are lined up using their innovation number. For wach innovation number the gene from the most fit parent is selected and inserted into the child genome. If both parent genomes are the same fitness then the gene is randomly selected from either parent with equal probability. If the innovation number is only present in one parent then thi is known as a disjoint or excess gene and represents a topological innovation, this too is inserted into the child.
- Los genes de los genomes se alínean usando el innovation number y se elige el gene más fuerte de entre los alineados

#### Speciation

Speciation takes all the genomes in a given genome pool and attempts to split them into distinct groups known as species. The genomes in each species will have similar characteristics.  A way of measuring the similarity between two genomes is required, if two genomes are "similar" thy are from the same species. A natural measure to use would be a weighted sum of the number of disjoint & excess genes (representing topological differences) and the difference in weights between matching genes. If the weighted sum is below some threashold then the genomes are of the same species.

The advantage of splitting the genomes into species is that during the genetic evolution step where genomes with low fitness are culled (removed entirelly from the genome pool) rather than having each genome fight for it's place against every other genome in the entire genome pool we can make it fight for it's place against genomes of the same species. This way species that form from a new topological innovation that might not have a high fitness yet due to not having it's weights optimised will survive the culling.

#### Summary of whole process
* Create a genome pool with n random genomes
* Take each genome and apply to proble / simulation and calculate the genome fitness
* Assign each genome to a species
* In each species cull the genome removing come of the weaker genomes
* Breed each species (randomly select genomes in the species to either crossover or mutate)
* Repeat all of the above

#### Implementatino to the pole balancing problem
The simulatio of the pole balancing requires 5 funtions:
- processInitialStateFunc - This specifies the initial state of the system, for the pole balance problem the state is the cart location, cart velocity, cart acceleration, force being applied to cart, pole angle, pole angular velocity and pole angular acceleration.
- processUpdateStateFunc - This specifies how to take the current state and update it using the outputs of the neural network. In the example this function simulates the equations of motion and takes the neural net output as the force that is being applied to the cart.
- processStateToNeuralInputFunc - Allows for modifying the state / normalisation of the state before it is passeed as an input to the nrural network.
- fitnessUpdateFunc - Takes the old fitness, the old state and the new updated state and determines what the new system fitness is. For the pole balance problem this function wants to reward the pendulum being up right, and reward the cart being close to the middle of the track.
- terminationCheckFunc - Takes the satte and checks to see if the termination should be terminated. Can chose to terminate if the pole falls over, the simulation has ran too long or the cart jass driven off the of the end of the track.
- poleStateFunc - Plots the state, for the pole balance this draws the cart and pendulum



## Pseudocódigo NEAT con XOR
- 












#### Recursos

https://lethain.com/genetic-algorithms-cool-name-damn-simple/

http://gekkoquant.com/2016/03/13/evolving-neural-networks-through-augmenting-topologies-part-1-of-4/

Reinforcement learning in autonomous agents
http://repositorio.ipl.pt/bitstream/10400.21/1144/1/Disserta%C3%A7%C3%A3o%20Ingl%C3%AAs 

