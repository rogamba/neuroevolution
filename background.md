# Antecedentes

## Notas
- Machine learning
  - Supervised learning -> function induction: from particular cases to general rule (labels data well)
  - Unsupervised learning -> description & clusterization (cluster that scores well)
  - Reinforcement learning -> (bahaviour that scores well) 
      - It is similar to supervised learning but instead of being proveided with the correct output for each network input, te algorithm is only given a grade. The grade is a measure of the network's performance.

## Temas
- Algebra lineal (matrices, multiplicación, tranpuesta, etc...)
  - Linear vector spaces, linear independence, basis, vector expansion, etc...
    - Dot product: Length of the projection of a vector in another vector
  - Matrices (x)
  - Matrix multiplication (x)
  - Transpose (x)
  - Inverse (x)
- Calculus
  - Chain Rule
  - Partial derivatives
  - Vectorial calculus
- Optimization algorithms
  - Error calculation
    - Minimum square errors: distance from point to the line, square and 
  - Gradient Descent
  - Newton
  - Conjugate Gradient
- Logistic regression
- Neural Networks
  - Transfer functions (de dónde viene la sigmoidal)
  - Perceptron
    - Perceptron learning rule
      - In supervised learning the learning rule is provided by a set of examples:
        [{p1,t1},{p2,t2},}{p3,t3},{p4,t4},...{pq,tq}]
        p: input --> t: correct output for the given input
      -  
  - Hamming
    - Designed to explicitly solve binary pattern recognition problems
    - It uses feedforward and recurrent (feedback) layers
    - Objective to decide which of the prototype vector is closest to the input vector
    - There is one neuron in the recurrent layer for each prototype pattern
  - Feedforward learning
  - Hopfield
  - Hebbian
  - Backbropagation
- Genetic algorithms
- NEAT Applications
- Modelado de un péndulo invertido
- Aplicación de neat para el control de un péndulo invertido
