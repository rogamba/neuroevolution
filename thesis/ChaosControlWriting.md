# Índice



* **Cap 1 Introducción**

  * Abstract
  * Motivación
  * Justificación
  * Objetvos

* **Cap 1 Antecedentes**
  * Sistemas dinámicos
      * Modelado
      * Lineabilidad
      * Estabilidad
      * Caos
  * Control
  * Inteligencia Artificial
  * Algoritmos Genéticos
      * Operadores genéticos
      * Implementación
  * Redes Neuronales
      * Aprendizaje supervisado
      * Aprendizaje no supervisado
      * Aprendizaje reforzado
  * Neuroevolución

* **Cap 2 NEAT**
    * Visión general
    * Encoding
    * Mutación
        * Mutación de pesos
        * Mutación de enlaces
        * Mutación de topología
    * Reproducción
    * Especiación
    * Parámetros de configuración

* **Cap 3 Modelo y Control**
    * Diagrama de cuerpo libre
    * Modelo
    * Análisis de estabilidad
    * Control neuroevolutivo

* **Cap 4 Diseño de Neuropy**
    * Visión general
    * Requerimientos
    * Tecnología
    * Proceso principal del sistema
    * Entidades
    * Paquetes
    * Arquitectura

* **Cap 5 Manos al código**
    * Metodología de desarrollo (Incremental)
    * Requerimientos técnicos
    * Instalación de Python en Linux/OSX
    * Ambiente de desarrollo
    * Módulos y clases
    * Estructura de archivos
    * Ejecución

* **Cap 6 Simulaciones y Resultados**
    * Función XOR
    * Péndulo simple
    * Péndulo doble

* **Cap 7 Conclusiones**




# Resumen



El presente trabajo propone el diseño y desarrollo de una aplicación del algoritmo NEAT (“Neuroevolution of Augmented Topologies” por sus siglas en inglés) para controlar un péndulo invertido doble, conocido también como péndulo caótico por su comportamiento estocástico. Existen diversos tipos de control automático para estabilizar este tipo de sistemas altamente no lineales como lo son: método de Lyapunov, variables de estado, lógica difusa, etcétera. Todos ellos requieren de un amplio dominio y entendimiento matemático y no todos ellos resuelven el problema eficientemente.

Con el avance de las tecnologías de la información e investigaciones en el area de la cognición y pensamiento humano, hemos llegado a desarrollar sistemas con la capacidad de aprender de la experiencia. Al proceso de aprendizaje basado en el comportamiento se le llama “aprendizaje reforzado” o reinforcement learning en inglés.
La capacidad del humano de adquirir conocimiento mediante la evaluación y evolución del comportamiento fue el punto de partida que se tomó para investigar el área del aprendizaje automático. Gracias a los estudios realizados en neurobiología, hemos podido reproducir (hasta cierto punto) la interacción que existe entre nuestras neuronas y así formular modelos matemáticos para implementarlos en diferentes tipos de aplicaciones. A estos modelos se les conoce hoy en día como redes neuronales artificiales.

El escaso entendimiento del comportamiento de las redes neuronales artificiales hace que su implementación y diseño sea una tarea complicada, es por esta razón que se ideó el algoritmo NEAT, el cual propone usar un algoritmo genético para evolucionar tanto pesos como topología de distintos diseños de redes neuronales artificiales, esto con el objetivo de llegar a la solución del problema propuesto.





# Introducción



## Abstract

En este proyecto, desarrollamos una implementación del algoritmo NEAT (Neuroevolution of Augmented Topologies) en python con el objetivo de llegar a controlar un péndulo invertido caótico. Probamos el algoritmo con tres distintos modelos para medir su eficiencia: aproximación de la función XOR, control de un péndulo invertido sencillo y control de un péndulo invertido doble sobre un carro. Después de varias pruebas variando los parámetros de configuración del algoritmo, pudimos obtener una solución para controlar el péndulo invertido doble sobre el carro móvil y así concluir que el algoritmo NEAT sirve para controlar por lo menos un tipo de sistema caótico.

## Motivación

Desde el inicio del estudio de mi carrera en ingeniería mecatrónica, me ha llamado mucho la atención las distintas técnicas para representar, medir y controlar distintos sistemas físicos. Desafortunadamente para algunos de nosotros, el comportamiento de la gran mayoría de los sistemas que nos rodean es altamente no lineal y por lo tanto su modelado y control se vuelven sumamente complejos si se quiere tener un nivel de precisión considerable. Al realizar algunas investigaciones en el tema, encontré una aplicación de las redes neuronales para el control de sistemas no lineales, esto es básicamente un sistema que “aprenda” de la experiencia y así evoluciona modificando su comportamiento y mejorando su desempeño hasta que se vuelva un “experto” en la solución del problema.

[Imitación de la naturaleza con evolución y redes neuronales]

## Justficación

[...]

## Objetivos

### Generales

* Estudiar e investigar distintas implementaciones del algoritmo NEAT (Neuroevolution of Augmented Topologies) en python y otros lenguajes de programación para detectar areas de oportunidad.
* Desarrollar el modelo matemático del péndulo invertido doble sobre un carro y desarrollar módulos en python para su simulación
* Desarrollar propuesta de implementación del algoritmo NEAT en python para controlar modelo del péndulo invertido doble sobre un carro y medir resultados de entrenamiento.

### Particulares

- Aplicar los conocimientos adquiridos ee algebra lineal, calculo vectorial y métodos numéricos, para entender más a fondo el comportamiento y modelado de las redes neuronales artificiales y algoritmos genéticos.
- Modelar el sistema del péndulo caótico sobre un carro en dos dimensiones mediante el método de Lagrange.
- Medir los resultados de la implementación del algoritmo y compararlos con un control no lineal (PID).
- Realizar la simulaciones del control neuroevolutivo entrenado con diversos parámetros y medir resultados.





# Antecedentes



## Sistemas dinámicos

Para entender los sistemas dinámicos es importante primero definir un sistema. Un sistema es simplemente un conjunto de elementos en interacción. Un sistema dinámico se refiere a un sistema en el cual una función describe la dependencia temporal de un punto en el espacio, esto quiere decir que un sistema dinámico es un sistema que varía su estado en el tiempo y que a su vez su estado puede ser descrito en el espacio. Algunos ejemplos de sistemas dinámicos incluyen: modelos matemáticos que describen el comportamiento del vaivén de un péndulo, el flujo de agua a travez de una pipa o incluso el incremento de la población de una especie a lo largo del año.

### Sistema

El control automático ha tomado un rol de vital importancia en el avance de la ciencia y la ingeniería. El control en términos generales es la serie de operaciones necesarias que se deben de realizar a partir del estado de un sistema para que éste se comporte de la manera que deseamos. Para llegar a este objetivo se debe de tener muy claro el comportamiento del sistema que se quiere controlar, éste puede o no ser un sistema físico pero en nuestro caso nos enfocaremos únicamente a estos. Para llegar al modelo matemático de un sistema, se deben utilizar diversas herramientas matemáticas que en última instancia pretenden simular el comportamiento del sistema en términos cuantificables.

## Control

La rama de control automático es escencial en cualquier área de ciencias e ingeniería. La teoría de control se encarga del comportamiento de los sistemas dinámicos y de cómo modificar su estado para que tenga una respuesta deseada.    Por ejemplo, el control es una parte primordial en sistemas de vehículos espaciales, sistemas robóticos, sistemas de manufactura moderna y cualquier proceso industrial en el cual se requiera controlar temperatura, presión, humedad, cantidad de flujo, etc. Dado el alcance del proyecto nos limitaremos a explicar brevemente tres tipos de control: teoría de control clásico y moderno.

### Control Clásico
La teoría de control clásico se enfoca en controlar un sistemas, regularmente llamado "planta", para que sus salida sigan el valor de una señal controlada deseada llamada referencia, éste puede ser un valor estático o que cambie con el tiempo. Para llegar a este objetivo se diseña un controlador, el cual monitorea la salida del sistema y la compara con el valor de referencia. La diferencia entre la salida y el valor de referencia (la salida deseada), es la señal de error la cual es aplicada como realimentación a la(s) variable(s) entrada del sistema para acercar el valor de salida al de referencia, queriendo lograr así la reducción del error.

El control clásico se enfoca en sistemas lineales, invariantes en el tiempo de una sóla entrada y una sóla salida. (Desafortunadamente para nosotros, existen muy pocos sistemas así en la naturaleza y por lo tanto su aplicación es limitada.) Se utiliza la transformada de Laplace para cambiar una Ecuación Diferencial Ordinaria en el dominio del tiempo a un polinomio algebráico regular en el dominio de la frecuencia. Una vez que el sistema ha sido transformado, éste se puede manipular con mucho mayor facilidad.

[Ejemplos]

### Control Moderno

La tería moderna de control se basa en el análisis del dominio del tiempo de sistemas de ecuaciones diferenciales. En la teoría de control moderno, en vez de cambiar de dominio para evitar complejidades matemáticas, convierte las ecuaciones diferenciales ordinarias en un sistema de ecuaciones en el dominio del tiempo de menor orden llamado ecuaciones de estado, el cual puede ser manipulado usando técnicas de álgebra lineal.

[Ejemplos]


## Inteligencia artificial



- Avences y resurgimiento, aplicaciones
- Historia del surgimiento y matemáticas
- Prueba de touring
  [Herramientas]
  Algunas herramientas matemáticas
  [Aprendizaje de máquina]
  Una de las áreas con mayor interés por parte de los investigadores y con más aplicaciones a nivel de industria y negocios es el del aprendizaje de máquina. Esta área de investigación nace d la idea de crear un sistema con la capacidad de "aprender" de un set de información determinado, o de adaptarse y aprender de información nueva.


[Aplicaciones AI]
Algunas de las aplicaciones más relevantes hoy en día de la inteligencia artificial incluyen:
* Procesamiento natural de lenguaje
* Sistemas expoertos
* Sistemas de visión
* Reconocimiento del habla
* Robótica
* Minería de datos
* Sistemas de recomendación







En los últimos años, el término de inteligencia artificial ha ganado mucha fuerza debido al incremento de sus aplicaciones en todas las áreas. Desde detección de fraudes, identificación de rostros, hasta predicción de precios de las acciones en la bola de valores. El término "inteligencia artificial" se refiere al comportamiento inteligente de una máquina,

El área de investigación en inteligencia artificial ha recobrado popularidad en el sigo XX debido al avance en el poder de procesamiento de

cosa que hace apenas unos años se consideraba ciencia ficción, al día de hoy hemos programado sistemas que pueden pasar l

En los últimos años e término
En los últimos años, el término de "inteligencia artificial" se ha escuchado cada vez
Para entender a fondo el trabajo presentado es importante conocer en términos generales a que nos referimos cuando

## Algoritmos genéticos

Los algoritmos genéticos están inspirados en el concepto de la selección natural de Darwin para la discriminación de soluciones de una función dada. De forma general, el objetivo de utilizar de un algoritmo genético es encontrar una solución óptima a un problema, definiendo una función de aptitud con la cual serán evaluadas las diversas soluciones. Para encontrar la solución óptima se itera a través de múltiples generaciones que estarán consutuidas por varios "individuos", mismos que hacen referencia a la evaluación de la función que se quiere optimizar con distintos parámetros. Los individuos de la generación son evaluados en base a la función de aptitud, las soluciones con mejor desempeño serán seleccionadas para recombinarse y mutarse para producir soluciones hijo, que a su vez volverán a ser evaluadas en un proceso iterativo hasta encontrar la mejor solución posible.

Los algoritmos genéticos fueron inventados por John Holland en los años 60s y fueron desarrollados por Holland y sus estudiantes y colegas en la universidad de Michigan entre los años 1960s y 1970s. En contraste con las estrategias de evolución y la programación evolutiva, la meta principal de Holland no era diseñar un algoritmo para resolver problemas específicos, sino estudiar formalmente el fenómeno de adaptación como sucede en la naturaleza y así desarrollar formas en las que los mecanismos de adaptación natural puedan ser representados en sistemas computacionales. Holland presentó los algoritmos genéticos en su libro sobre los sistemas naturales y artificiales como una abstracción de la evolución biológica y dio un marco teórico para la adaptación.

### Operadores genéticos

La forma más simple de algoritmos genéticos involuran tres tipos de operadores: selección, combinación y mutación.

* **Selección**: Este operador selecciona el cromosoma en la población para la reproducción. Entre más apto sea el chromosoma, es más probable que sea seleccionado más veces para su reproducción.
* **Combinación**: Este operador selecciona de manera aleatoria un lugar e intercambia subsecuencias antes y después del lugar entre dos cromosomas para crear dos hijos. Por ejemplo, las cadenas de caracteres 10000100 y 11111111 podrías ser combinadas después del lugar en cada uno para producir los hijos 10011111 y 11100100. El operador de combinación imita de manera muy general la combinación entre dos organismos de cromosomas simples (haploide).
* **Mutación**: Este operador modifica de manera aleatoria algún bit en el chromosoma con el objetivo de asegurar la diversidad de individuos en la generación. Por ejemplo, la cadena 00000100 podría ser mutada en su segunda posición para producir 01000100. La mutación puede ocurrir en cada posición de bit según una probabilidad definida, normalmente muy reducida (ej. 0.001).

### Implementación

El algoritmo más simple de su implementación es el siguiente:

1. Generar una población aleatoria de n cantidad de cromosomas o individuos (soluciones candidato).
2. Calcular el desempeño del cada cromosoma x en la población
3. Repetir lo siguiente hasta que n descendientes han sido calculados:
    1. Selección de un par de cromosomas, la probabilidad de la selección es una función de su desempeño. El mismo cromosoma puede ser seleccionado más de una vez para ser padre.
    2. Con la probabilidad de combinación (pc), entrecruzamos el par de cromosomas en un punto aleatorio, Si no hay combinación los descendientes serán copias exactas de alguno o ambos padres.
    3. Mutar los descendientes dada una probabilidad de mutación (pm), y colocar los cromosomas resultantes en la nueva población
4. Reemplazar la población actual con la nueva
5. Repetir el paso 2


## Redes neuronales artificiales

El concepto de las redes neuronales artificiales nace de la idea de imitar el comportamiento de las neuronas bioógicas y ha estado en el aire desde 1940 con el trabajo de Warren McCulloch y Walter Pits, quienes pudieron demostrar que redes de neuronas artificiales podían procesar cualquier función aritmética o lógica. Luego de ellos surgieron varios investigadores quienes realizaorn grandes aportes al área como Donald Hebb, Frank Rosenblatt, Bernard Widrow, John Hopfield, por mencionar algunos.

Las redes neuronales artificiales están confirmadas por una o varias neuronas artificiales, éstas pretenden imitar el comportamiento de una neurona biológica la cual consta de las siguientes partes:


### Tipos de entrenamiento

### Tipos de redes

Existen diversos tipos de clasificar las redes neuronales artificiales,





# Cap 2 NEAT



## Visión general

El algoritmo NEAT (Neuroevolución por Topologías Aumentadas) fue desarrollado por Kenneth O. Stanley en el año 2002 y propone, en términos generales, la implementación de un algoritmo genético para evolucionar generaciones de redes neuronales y llegar así a una solución óptima.

El objetivo principal del algoritmo de Neuroevolución por Topologías Aumentadas (NEAT) es desarrollar un algoritmo genético que busque la solución óptima de una red neuronal modificando tanto sus pesos como su topología, tiene las siguientes propiedades:

1. Existe una representación genética de la red que permite que la combinación de las estructuras sea de una manera significativa
2. Protege la innovación topológica que necesita evolucionar algunas generaciones para ser optimizada y así no desaparezca del pool de geneomas prematuramente
3. Minimizar topologías mediante entrenamiento y funciones de penalización

### Terminología

**Gen**: Cada individuo o red consta de dos tipos de genes: genes de nodos y genes de conexiones
**Genoma**: Individuo
**Fenotipo**: Representación "real" de un genotipo - Network
**Población**:  Hace referencia a todos los individuos de una generación
**Marcadores históricos**:  Permiten saber el orígen de un gen y ayuda a resolver el problema de convenciones competidoras


## Encoding

[Existen diversos métodos para codificar y representar la topologiá de una ANN, en el caso del algoritmo NEAT, Stanley decidió utilizar la codificación binaria o directa ya que se necesita seguir el rastro de la evolución de la topología de una red, cosa que con codificación indirecta no se podría dado que es posible representar distintas soluciones con u
El método de codificación directa es ilustrada en el trabajo de Geoffrey Miller, Peter Tod, and Shilesh Hedge (1989), quienes restringieron sus]

La representación del genotipo tiene una codificación directa, esto quiere decir que el número de nodos, conexiones y pesos de un genotipo está extresado de manera literal en la representación. Por ejemplo el encoding de la red neuronal solución al problema de OR exclusivo con el mínimo número de neuronas es:

Inputs: Todos son numerados con entedos negativos, desde -1 hasta -inf. No son considerados dentro de los nodos de la clase de genotipo porque no tienen sesgo ni activación

![xor](./thesis-images/neat/neat_net_xor.png)

```python
Genome(
    nodes=[
        { "key" : 0, "bias" : b0, "activation" : "sigmoid"}         # Output
        { "key" : 1, "bias" : b1, "activation" : "sigmoid"}         # Hidden neuron
    ],
    links=[
        { "key" : (-2,0), "weight" : w0, "enabled" : True }
        { "key" : (-2,1), "weight" : w1, "enabled" : True }
        { "key" : (-1,0), "weight" : w2, "enabled" : True }
        { "key" : (-1,1), "weight" : w3, "enabled" : True }
        { "key" : (1,0), "weight" : w4, "enabled" : True }
    ]
)
```

**Genes de Nodos**
{ key=0, bias=-1.43, resp=1.0, activation=sigmoid }   -   Neurona de salida
{ key=1, bias=-2.3, resp=1.0, activation=sigmoid }    -   Neurona de la capa oculta
(representa dos neuronas con sesgo de -1.43 y -2.3, ambas con función de activación sigmoidal)

**Genes de Conexiones**
{ key=(-2,0), weight=2.5, enabled=True }              -   Neurona de input B a salida
{ key=(-2,1), weight=-2.31, enabled=True }              -   Neurona de input A a salida
{ key=(-1,0), weight=-2.67, enabled=True }              -   Neurona de input B a salida
{ key=(-1,1), weight=4.04, enabled=True }              -   Neurona de input B a salida
{ key=(1,0), weight=6.155, enabled=True }              -   Neurona de input B a salida

## Convenciones competidoras

Convenciones competidoras significa tener más de una manera para expresar una misma solución a un proplema de optimización de pesos con una red neuronal. Cuando los genomas representando la misma solución no tienen el mismo encoding, el cruzamiento puede producir descendientes o "hijos" defectuosos. El principal indeicador en NEAT es que el origen histórico de dos genes, es evidencia directa de la homología si los genes comparten el mismo origen. Por lo tanto, NEAT realiza sinápsis artificiales basadas en marcadores históricos, permitiendo cambiar la topología de la red sin perder la pista de cada gen en el curso de una simulación.
- Más de una manera de expresar una misma solución de optimización de pesos
- NEAT lo resuelve colocando marcadores históricos a los genes, permitiendo cambiar la estructura de la red sin perder la pista de los genes a lo largo de la simulación


## Mutación

### Mutación de pesos
[Mutación no estructural]

### Mutación de nodos
[Mutación estructural]
Siempre que se agrega un nodo en la red, se hace añadiéndolo entre dos nodos conectados, asignando peso de 1 para que no haya tanta afectación al sistema. Por lo tanto cuando se hace una mutación de nodo se agregan 3 genes: un gen de nodo y dos genes de conexión. Tomemos como ejemplo una red simple a la que se le quiere agregar un nuevo nodo. Los genes quedarían de la siguiente manera:

Ejemplo de agregar un nodo entre Nodo0 (output) y NodoC (input):
- Se deshabilita el link entre los nodos 0  y C
- Se agrega el nuevo nodo (Nodo2)
- Se agrega gen de conexión entre NodoC y Nodo2 con peso de 1
- Se agrega gen de conexión entre Nodo2 y Nodo0 con el peso que tiene el gen deshabilitado entre Nodo0 y NodoC

**Antes de la mutación**
![node_mutate1](./images/neat_mutate_node_1.png)
```python
Genome(
    nodes=[
        { "key" : 0, "bias" : b0, "activation" : "sigmoid"},    # Output
        { "key" : 1, "bias" : b1, "activation" : "sigmoid"}     # Hidden
    ],
    links=[
        { "key" : (-3,0), "weight" : w0, "enabled" : True  },
        { "key" : (-2,1), "weight" : w1, "enabled" : True  },
        { "key" : (-1,1), "weight" : w2, "enabled" : True  },
        { "key" : (-1,0), "weight" : w3, "enabled" : True  },
        { "key" : (1,0), "weight" : w4, "enabled" : True  },
    ]
)
```
**Después de la mutación**
![node_mutate2](./images/neat_mutate_node_2.png)
```python
Genome(
    nodes=[
        { "key" : 0, "bias" : b0, "activation" : "sigmoid"},    # Output
        { "key" : 2, "bias" : b1, "activation" : "sigmoid"}     # Hidden
        { "key" : 2, "bias" : b2, "activation" : "sigmoid"}     # New Hidden
    ],
    links=[
        { "key" : (-3,0), "weight" : w0, "enabled" : False  },  # Deshabilitado
        { "key" : (-2,1), "weight" : w1, "enabled" : True  },
        { "key" : (-1,1), "weight" : w2, "enabled" : True  },
        { "key" : (-1,0), "weight" : w3, "enabled" : True  },
        { "key" : (1,0), "weight" : w4, "enabled" : True  },
        { "key" : (-3,2), "weight" : 1, "enabled" : True  },    # Peso de 1 innov+1
        { "key" : (2,0), "weight" : w0, "enabled" : True  }     # Peso de C a 0 innov+2
    ]
)
```


### Mutación de enlaces
[Mutación estructural]


## Reproducción

Dos genes con el mismo origen histórico deben representar la misma estructura (aunque posiblemente con pesos distintos), pues los dos son derivados del mismo gen ancestro de algún punto en el pasado. Por lo tanto la necesidad de alinear los genes entre ellos es para seguir la pista del origen histórico de cada gen del sistema.
Trackear el origen histórico requiere muy poca computación. Cuando un nuevo gen aparece (por medio de mutación estructural), un __global innovation number__ se incrementa y se asigna a ese **gen**. El innovation number por lo tanto representa una cronología de las apariciones de cada gen en el sistema

* Cuando se agrega un nuevo gen al genoma por mutación estructural (node/link mutation), se incrementa un global innovation number y se le asigna al nuevo gen creado. Esto quiere decir que todos los genes.
  Un problema que se puede presentar es que la misma innovación estructural reciba diferentes __innovation numbers__ en la misma generación

Para hacer la recombinación entre los genomas, éstos de alinean conforme el innovation number de los genes y considerando únicamente los genes de las conexiones. Por ejemplo si un genotipo tiene un innovation number y otro no, se considera un __disjoint__ (desarticulación) si está dentro del rango del innovation number de ambos, si no está dentro del rango del innovation number de alguno, éste se considera un __excess__ (exceso).
Cuando se conforma el genoma hijo, los genes son seleccionados aleatoriamente de cualquier padre en los genes alineados, y los genes desarticulados o en exceso siempre son incluidos del padre más apto (con mejor desempeño).

* Los genes que están alineados se heredan de manera aleatoria
* Los genes no alineados (desarticulados o en exceso) son heredados del padre más apto (todos). Si el desempeño de ambos es igual se realiza de manera aleatoria

**Ejemplo de cruzamiento entre genomas**:
```python
# Padre 1
parent1 = Genome(
    nodes=[
        { "key" : 0, "bias" : b0, "activation" : "sigmoid"},
        { "key" : 1, "bias" : b1, "activation" : "sigmoid"}
    ],
    links=[
        { "key" : (-3,0), "weight" : w10, "enabled" : True  },
        { "key" : (-2,1), "weight" : w11, "enabled" : True  },
        { "key" : (-1,1), "weight" : w12, "enabled" : True  },
        { "key" : (-1,0), "weight" : w13, "enabled" : True  },
        { "key" : (1,0), "weight" : w14, "enabled" : True  },
    ]
)
# Padre 2
parent2 = Genome(
    nodes=[
        { "key" : 0, "bias" : b0, "activation" : "sigmoid"},
        { "key" : 1, "bias" : b1, "activation" : "sigmoid"},
        { "key" : 2, "bias" : b2, "activation" : "sigmoid"}
    ],
    links=[
        { "key" : (-3,1), "weight" : w20, "enabled" : True  },
        { "key" : (-3,0), "weight" : w21, "enabled" : True  },
        { "key" : (-2,1), "weight" : w22, "enabled" : True  },
        { "key" : (-1,2), "weight" : w23, "enabled" : True  },
        { "key" : (-1,0), "weight" : w24, "enabled" : True  },
        { "key" : (1,2), "weight" : w25, "enabled" : True  },
        { "key" : (2,3), "weight" : w26, "enabled" : True  }
    ]
)
```

**Hijo**
```python
# Padre 2
parent2 = Genome(
    nodes=[
        { "key" : 0, "bias" : b0, "activation" : "sigmoid"},   # Random
        { "key" : 1, "bias" : b1, "activation" : "sigmoid"},   # Random
        { "key" : 2, "bias" : b2, "activation" : "sigmoid"}    # Random
    ],
    links=[
        { "key" : (-3,1), "weight" : w20, "enabled" : True  }, # Parent 2
        { "key" : (-3,0), "weight" : w21, "enabled" : True  }, # Parent 2
        { "key" : (-2,1), "weight" : w11, "enabled" : True  }, # Parent 1
        { "key" : (-1,2), "weight" : w23, "enabled" : True  }, # Parent 2
        { "key" : (-1,1), "weight" : w12, "enabled" : True  }, # Parent 1
        { "key" : (-1,0), "weight" : w13, "enabled" : True  }, # Parent 1
        { "key" : (1,2),  "weight" : w25, "enabled" : True  }, # Parent 2
        { "key" : (2,0),  "weight" : w20, "enabled" : True  }  # Parent 2
    ]
)
```

## Especiación

Frecuentemente, agregar nodos en una red hace que inicialmente baje su desempeño antes de permitir que sus pesos se optimicen. Por ejemplo aumentar la estructura introduce no linealidades en donde no las había antes. Es improbable que un nuevo nodo o conexión expresen una buena respuesta tan pronto como son introducidos a la red. Desafortunadamente, como inicialmente hay una pérdida de desempeño del genoma, la innovación es improbable que sobreviva en una población que ya se ha optimizado previamente. Por tal motivo, es necesario  proteger de alguna manera las innovaciones estructurales de la red para que puedan hacer uso y optimizar esa  nueva estructura.

En la naturaleza, diferentes estructuras tienden a pertenecer a diferentes grupos o especies que compiten en diferentes nichos. Por lo tanto, innovación es implícitamente protegida dentro del nicho. De manera similar, si las redes con estructuras novedosas pudieran estar aisladas en su propia especie, tendrían oportunidad de optimizar sus estructuras antes de tener que competir con el grueso de población.

La especiación requiere de una función de compatibilidad que indique si dos genomas deberían estar en la misma especie o no. Es difícil formular esta función de compatibilidad entre redes de distintas topologías.
Como NEAT guarda información histórica del gen, la población en NEAT puede ser fácilmente separado en especies, Se usa la división explícita de desempeño (explicit fitness sharing), la cual forza individuos con genomas similares a compartir su desempeño.

Crear especies dentro de la población permite que los organismos compitan primordialmente con los de su mismo nicho en vez de competir con el grueso de la población. La idea es dividir la población en especies, de tal forma que las topologías similares queden en la misma especie. Parecería que esta acción corresponde enteramente a un pareo de topologías, sin embargo los marcadores históricos proveen una solución efficiente al problema. Entre más desarticulados sean dos genomas, menos historia de evolución comparten y por lo tanto son menos compatibles entre sí. Podemos medir la distancia de compatibilidad d de distintas estructuras como una simple combinación lineal del número de genes de excesos y desarticulaciones , así como el promedio de la diferencia entre pesos de los genes pareados (alineados), incluyendo los genes deshabilitados.

```python
d = c1*(E/N) + c2*(D/N) + c3*avg(W)
```
**d**: distancia de compatibilidad
**E** : número de genes de exceso
**D** : número de genes desarticulados
**W** : pesos de la red
**ci** : coeficientes para ajustar la importancia de los tres factores
**N** : número de genes en el genoma más grande


La distancia de compatibilidad nos permite comparar contra un umbral de compatibilidad definido por nosotros. En cada generación, los genomas son sequencialmente incluidos en las especies.
Cada especie tiene un organismo representante que es seleccionado de manera aletoria de la generación anterior.
Un genoma g en la generación en curso es colocado en la primer especie con la cual sea compatible, midiendo la distancia entre él y cada representante de esa especie. De esta manera las especies no se traslapan. Si g no es compatible con ningún representante de las especies, una nueva especie es creada

Fitness sharing:

```python
adjusted_fitness_j = fitness_j / sum( sh(d(i,j)), range(i,n) )

# Otra forma de expresarlo
adjusted_fitness_j = fitness_j / num_organisms_same_species_of_j
```
**j**   : número de genoma específico
**i**   : número total de neuronas
**d**   : función que regresa la distancia de compatibilidad entre i y j
**sh**  : sharing function  (regresa 1 o 0)

**Sharing function**
Si la distancia entre el genoma i y j es mayor que el umbral de compatibilidad, la función regresa 0. Si la distancia es menor que el umbral, la función regresa 1.
* Por ejemplo si hay una especie con un sólo individuo la función de sharing sería:
```
adjusted_fitness = fitness / 1
```
**(Por ejemplo si hay una especie con 3 individuos, se divide entre 3)**

Las especies entonces se reproducen primero eliminando los genomas menos aptos de la población. La población entera es reemplazada por los hijos de ls organismos reminentes de cada especie.

Importante:
* El número de excesos y desarticulaciones entre un par de genomas es una medida natural para medir su compatibilidad.
* Se toma un representante aleatorio de la especie de la generación previa
* Para colocar a un organismo en una especie se mide la distancia vs el representante de cada especie, si es compatible se asigna a esa especie
* Si el genoma a especiar no es compatible con ningún representante, se crea una nueva especie
* El fitness sharing entre individuos de la misma especie tiene como propósito penalizar a las especies con muchos individos, así una sola especie no toma todo el control
* Fitness sharing function: fitness = fitness / num_organismos_de_la_especie
* Toda la especiación se reduce a ajustar el fitness considerando el número de individuos parecidos

## Población inicial

En NEAT, a diferencia de otros algoritmos de neuroevolución, establece  los individuos de su población inicial con pocas neuronas y las cuales están interconectadas entre sí, sin capas de neuronas oculatas.
Una manera de minimizar las redes, es incorporando el tamaño de la red a la función de desempeño. En tales casos, el desempeño será penalizado para las redes más grandes.
La similaridad puede ser fácilmente medida basado en información histórica en los genes.
Una alternativa para no tener que modificar la función de desempeño, es que el mismo método de neuroevolución tienda a la minimalidad. Si la población empieza sin nodos ocultos y la estructura crece sólo conforme beneficie a la solución, no hay necesidad de modificar la función de desempeño. Por lo tanto, empezar con una población minimalista e ir creciendo su estructura es un principio de diseño en NEAT.

## Parámetros de configuración

Los parámetros de configuración que se pueden variar en el algoritmo NEAT y en específico de la implementación de Neuropy para obtener una convergencia más rápida y simplemente mejores resultados son:

```
# Número máximo de generaciones que correrá el algoritmo
generations: <int>

# Criterio de desempeño para seleccionar los individuos
fitness_citerion: <str: max|min>

# Umbral de desempeño para verificar si se ha encontrado una solución
fitness_threshold: <float>

# Tamaño de la población
pop_size: <int>

# Función de activación a utilizar
activation_default: <str: sigmoid|other>

# Funció nde agregación a utilizar
aggregation_default: <str: sum|other>

# Valor inicial del sesg o
bias_init_mean: <float>

# Valor inicial de la desviación estándar del sesgo
bias_init_stev: <int>

# Valor máximo del sesgo
bias_max_val: <int>

# Valor mínimo del sesgo
bias_min_val: <int>

# Probabilidad de mutació del sesgo
bias_mutate_power: <float>

# Tasa de mutación del sesgo
bias_mutate_rate: <float>

# Tasa de reemplazo del sesgo
bias_replace_rate: <float>

# Coeficiente de compatibilidadd entre especies
compatibility_disjoint_coefficient: <float>

# Coeficiente de compatibilidad entre especies
compatibility_weight_coefficient: <float>

# Probabilidad de agregar nuevo nodo
node_add_prob

# Probabilidad de borrar el nodo
node_delete_prob
* edge_add_prob
* edge_delete_prob
* enabled_default
* enabled_mutate_rate
* net_type
* initial_connections
* num_inputs
* num_outputs
* num_hidden
* weight_init_mean
* weight_init_stdev
* weight_max_val
* weight_min_val
* weight_mutate_power
* weight_mutate_rate
* weight_replace_rate
* compatibility_threshold
* species_fitness_function
* max_stagnation
* species_elitism
* elitism
* survival_threshold
* response_init_mean
* response_init_stdev
* response_max_val
* response_min_val
* response_mutate_power
* response_mutate_rate
* response_replace_rate

```




# Cap 3 Modelo y Control


## Diagrama de cuerpo libre
##Modelo

Nomenclatura 

$X$		Posición en el eje X del carro

$\theta_1$		Posición angular del péndulo 1

$\theta_2$		Posición angular del péndulo 2

$m_i$		Masa del elemento i

$I_i$		Momento de inercia del eslabón i

$g$		Constante de gravedad [9.81 $m/s^2$]

$l_i$		Distancia de la junta al centro de masa del péndulo i

$L_i$		Longitud del péndulo i

$u$		Fuerza controlada

$$L$$		Lagrangiano

$$T$$		Energía cinética del sistema

$$P$$		Energía potencial del sistema



#### Lagrangiano

Se propone el método de Lagrange para obtener el modelo que describe el cambio de energía del sistema en base a las posiciones angulares de los péndulos y la posición horizontal del carro
$$
\frac{d}{dt} ( \frac{\partial L}{\partial \dot{\theta}} ) - \frac{\partial L}{\partial \theta} = Q
$$
Donde $L = T - P$ es el Lagrangiano, $Q$ es el vector generalizado de fuerzas (o momentos) actuando en dirección de las coordenadas generalizadas $\theta$. 

Las energías cinéticas y potenciales del sistemas están dadas por la suma de las energías de sus componentes individualmente (un carro y dos péndulos).
$$
 T = T_0 + T_1 + T_2 \\
 L = L_0 + L_1 + L_2
$$
Energía cinética del carro:
$$
T_0 = \frac{1}{2} m_0 \dot{X}^2
$$
Energía cinética del péndulo 1:
$$
T_1 = \frac{1}{2} m_1 [( \dot{X} + \dot{\theta_0} l_1 \cos\theta_1 )^2 + (\dot{\theta_1} l_1 \sin\theta_1)^2] + \frac{1}{2} I_1 \dot{\theta_1}^2 \\
T_1 = \frac{1}{2} m_1 [(\dot{X}^2 + 2 \dot{X} \dot{\theta_1} \cos\theta_1 + \dot{\theta_1}^2 l_1^2 \cos^2\theta_1 ) + (\dot{\theta_1}^2 l_1^2 \sin^2\theta_1)] \\
T_1=\frac{1}{2} m_1 \dot{X}^2  + m_1 \dot{X} \dot{\theta} l_1 \cos\theta_1
 + \frac{1}{2} m_1 \dot{\theta_1}^2 l_1^2 \cos^2\theta_1 + \frac{1}{2} m_1 \dot{\theta_1}^2 l_1^2 \sin^2\theta_1 + \frac{1}{2} m_1 I_1 \dot{\theta_1}^2 \\
 T_1 = \frac{1}{2} m_1 \dot{X}^2  + \frac{1}{2} m_1 \dot{\theta_1}^2 l_1^2 + m_1 \dot{X} \dot{\theta_1} l_1 \cos\theta_1 + \frac{1}{2} m_1 I_1 \dot{\theta_1}^2
$$

Energía cinética del péndulo 2:
$$
T_2 = \frac{1}{2}m_2[(\dot{X} + \dot{\theta_1}  L_1 \cos{\theta_1} + \dot{\theta_2}l_2\cos{\theta_2})^2 + (\dot{\theta_1} l_1 \sin{\theta_1} + \dot{\theta_2} l_2 \sin{\theta_2})^2] + \frac{1}{2}I_2 + \dot{\theta_2} \\

T_2 = \frac{1}{2}m_2[(\dot{X}^2 + \dot{\theta_1}^2 L_1^2 \cos^2{\theta_1} + 2 \dot{X} \dot{\theta_1} L_1 \cos{\theta_1} + 2  \dot{X} \dot{\theta_2} l_2 \cos{\theta_2} + 2 \dot{\theta_1} \dot{\theta_2} L_1 l_2 \cos{\theta_1} \cos{\theta_2} + \dot{\theta_2}^2 l_2^2 \cos^2{\theta_2}) ...\\ + (\dot{\theta_2}^2 L_2^2 \sin^2{\theta_1} + \dot{\theta_1} \dot{\theta_2} L_1 l_2 \sin{\theta_1} \sin{\theta_2} + \dot{\theta_2}^2 l_2^2 \sin^2{\theta_2})] + \frac{1}{2} I_2 \dot{\theta_2}^2 \\

T_2 =\frac{1}{2}m_2[ \dot{\theta_1}^2 L_1^2 (\cos^2{\theta_1} + \sin^2{\theta_1}) + \dot{\theta_2}^2 l_2 (\cos^2{\theta_2} + \sin^2{\theta_2}) + 2 \dot{\theta_1} \dot{\theta_2} L_1 l_2 (\cos{\theta_1}\cos{\theta_2} + \sin{\theta_1}\sin{\theta_2})... \\  + 2 \dot{X}( \dot{\theta_1} L_1 \cos{\theta_1} + \dot{\theta_2} l_2 \cos{\theta_2} ) + \dot{X}^2 ] + \frac{1}{2} I_2 \dot{\theta_2}^2 \\

T_2 = \frac{1}{2} m_2 [ \dot{\theta_1}^2 L_1^2 + \dot{\theta_2}^2 l_2^2 + 2\dot{\theta_1} \dot{\theta_2} L_1 l_2 \cos{(\theta_1-\theta_2)} + 2 \dot{X} \dot{\theta_1} L_1 \cos{\theta_1} + 2 \dot{X} \dot{\theta_2} l_2 \cos{\theta_2} + \dot{X}^2 ] + \frac{1}{2} I_2 \dot{\theta_2}^2 \\ 

T_2 = \frac{1}{2}m_2\dot{X}^2 + m_2\dot{\theta_1}\dot{\theta_2}L_1l_2\cos(\theta_1-\theta_2) + m_2\dot{X}\dot{\theta_1}L_1\cos{\theta_1}  + m_2\dot{X}\dot{\theta_2}l_2\cos{\theta_2} + \frac{1}{2}m_2\dot{\theta_1}^2 L_1^2  ...\\+ \frac{1}{2}\dot{\theta_2}^2 (m_2l_2 + I_2)
$$
Energías potenciales de los cuerpos
$$
P_0 = 0 \\
P_1 = m_1 g \cos{\theta_1} \\
P_2 = m_2 g (L_1 \cos{\theta_1}+ l_2 cos{\theta_2}) \\
$$
Obteniedo el Lagrangiano
$$
L = (T_0 + T_1 + T_2) - (P_0 + P_1 + P_2)
$$
Sustituyendo energías
$$
L = \frac{1}{2} m_0 \dot{X}^2 + \frac{1}{2}m_1\dot{X}^2 + m_1l_1\dot{X}\theta_1\cos{\theta_1} + \frac{1}{2}\dot{\theta_1}(m_1l_1^2 + I_1) ... \\ 
\frac{1}{2}m_2\dot{X}^2 + m_2\dot{\theta_1}\dot{\theta_2} L_1 l_2 \cos{(\theta_1-\theta_2)} + m_2\dot{X}\theta_1 L_1 \cos{\theta_1} + m_2\dot{X}\dot{\theta_2}l_2\cos{\theta_2} ... \\
\frac{1}{2}m_2\dot{\theta_1}^2L_1^2 + \frac{1}{2} \dot{\theta_2}^2(m_2l_2^2 + I_2) - m_1 g \cos{\theta_1} - m_2 g (L_1 \cos{\theta_1} + l_2 cos{\theta_2})
$$
Agrupando términos
$$
L = \frac{1}{2}\dot{X}(m_0 + m_1 + m_2) + \frac{1}{2}\dot{\theta_1}^2( m_1l_1^2+I_1+m_2 ) + \frac{1}{2}\dot{\theta_2}^2(m_2 l_2^2 + I_2 ) + \dot{X} \dot{\theta_1} \cos{\theta_1}(m_1l_1 + m_2L_1)\\
+ m_2\dot{X}\dot{\theta_2}l_2\cos{\theta_2} + m_2\dot{\theta_1}\dot{\theta_2}L_1l_2\cos(\theta_1 - \theta_2) - g\cos{\theta_1}(m_1l_1 + m_2L_2) - m_2gl_2\cos\theta_2
$$
Ecuaciones de Lagrange
$$
\frac{d}{dt} ( \frac{\partial L}{\partial \dot{X}} ) - \frac{\partial L}{\partial X} = u \\
\frac{d}{dt} ( \frac{\partial L}{\partial \dot{\theta_1}} ) - \frac{\partial L}{\partial \theta_1} = 0 \\
\frac{d}{dt} ( \frac{\partial L}{\partial \dot{\theta_2}} ) - \frac{\partial L}{\partial \theta_2} = 0
$$
Explícitamente
$$
u = (\sum{m_i})\ddot{X} + (m_1l_1 + m_2L_1)\cos(\theta_1)\ddot{\theta_1} + m_2 l_2\cos(\theta_2)\ddot{\theta_2} ...\\ -(m_1l_1+m_2L_1)\sin(\theta_1)\dot{\theta_1}^2 - m_2l_2\sin(\theta_2)\ddot{\theta_2}^2 \\
0 = (m_1l_1 + m_2L_1)\cos(\theta_1)\ddot{X} + (m_1l_1^2 + m_2L_1^2 + I_1)\ddot{\theta_1} + m_2L_1l_2\cos(\theta_1-\theta_2)\ddot{\theta_2} ...\\ + m_2L_1l_2\sin(\theta_1-\theta_2)\dot{\theta_2}^2  - (m_1l_1 + m_2L_1) g \sin\theta_1 \\
0 = m_2l_2\cos(\theta_2)\ddot{X} + m_2L_1l_2\cos(\theta_1-\theta_2)\ddot{\theta_1} + (m_2l_2^2+I_2)\ddot{\theta_2} ... \\ - m_2L_1l_2\sin(\theta_1-\theta_2)\dot{\theta_1}^2 - m_2l_2g\sin\theta_2
$$



##Análisis de estabilidad
##Control neuroevolutivo

La manera en la que se propone implementar el control neuroevolutivo al sistema es simplemente evaluando el tiempo de balanceo de los péndulos invertidos. Se definirán los límites de los ángulos permitidos para ambos péndulos, así como el desplazamiento permitido para el carrito antes de que se suspenda la evaluación del individuo en cuestión.  

En el modelo desarrollado para la simulación se le pueden modificar las siguientes variables para encontrar mejores resultados:

```python
class DoublePendulumCart(object):
	def __init__(self):
        self.g = -9.81 		# gravity constant
        self.m0 = 1.0 		# mass of cart
        self.m1 = 0.5 		# mass of pole 1
        self.m2 = 0.5		# mass of pole 2
        self.L1 = 1 		# length of pole 1
        self.L2 = 1 		# length of pole 2
        self.tau = 0.02 	# seconds between state updates
        self.force_mag = 40
```

Para llegar a la solución, es necesario evaluar cada una de los individuos de las diferentes generaciones, seleccionar a los más aptos, separarlos en especies y reproducirlos hasta llegar al umbral de desempeño definido en la configuración del algoritmo, en nuestro caso el umbral que definiremos para considerar una propuesta como "solución" al problema será de **10 segundos**, esto quiere decir que cualquier individuo dentro del proceso de evolución que llegue o sobrepase este valor de desempeño será seleccionado como la solución al problema y la simulación se detendrá.





# Cap 4 Diseño de Neuropy

## Visión general

Neuropy es una biblioteca en python que implementa una versión simplificada del algoritmo NEAT publicada por Kenneth O. Stanley in 2002, el cual, como se explicó en el capítulo anterior, propone un algoritmo genético para evolucionar redes neuronales artificiales. A grandes rasgos, consideraciones principales de este algoritmos contra otros algoritmos de aprendizaje reforzado son:

* Empieza el proceso de evolución con una población de individos con estructura o topología mínimas
* Protege innovaciones topológicas (nuevas estructuras de redes neuronales artificiales) por medio de la especiación de los individuos
* Usa marcadores históricos como una manera de resolver el problema de las convenciones competidoras, esto es que dos o más redes neuronales codificadas pueden representar la misma solución y por consiguiente podrían existir individuos que se repiten en una misma población.

El desarrollo de la biblioteca tiene como objetivo proporcionar un mejor entendimiento del algoritmo NEAT y está inspirada en la biblioteca **neat-python** por @CodeReclaimers con varias modificaciones con el fin de obtener mayor claridad en el código.

## Requerimientos generales

La lista de requerimientos del cliente para el desarrollo del paquete son los siguientes:

* Configuración de los parámetros del proceso de evolución por medio de un archivo de configuración `config.json`
* Inicio de ejecución del proceso de evolución por medio de línea de comandos, dando como argumento el modelo o clase a probar
* Indicar que se ha encontrado una solución cuando el desempeño de al menos un individuo sobrepasa el umbral de desempeño definido en el archivo de `config.json`
* Una vez terminado el proceso de evolución, imprimir el la red neuronal con mejor desempeño y guardarla en un archivo .json
* Posibilidad de evaluar la red solución a partir del archivo guardado
* Diseño de la estructura de archivos tal que sea fácil incluir nuevos modelos o clases para probar con el algoritmo

## Requerimientos técnicos

La lista de requerimientos para correr NeuroPy es:

* Python 3.5 o mayor
* OS Ubuntu / CentOS / Debian / macOS
* Memoria RAM de al menos 500Mb


## Tecnologías de Desarrollo

La librería fue desarrollada completamente en `python3.5`, el cual fue seleccionado por su simplicidad en la sintáxis, funcionalidad y gran comunidad como soporte. Para iniciar con el desarrollo, fue necesario en un principio crear un ambiente de desarrollo aislado para que no existiera conflicto entre las dependencias de python del sistema operativo y del nuevo paquete. Este ambiente de desarrollo es creado con el paquete de python llamado `virtualenv`.

La lista de requerimientos específicos de python que se utilizan en la ejecución de neuropy son:

* numpy: biblioteca especializada para el manejo eficiente de números y operaciones matemáticas en python
* graphviz: impresión y generación de la gráfica de la red neuronal artificial (grafo)
* matplotlib: impresión de la gráfica de la red neuronal artificial
* pyglet: biblioteca para generar la simulación de las clases del péndulo sencillo y doble


## Arquitectura

La arquitectura de directorios del paquete es la siguiente:

```python
/models
    ├── /double_pendulum.py
    ├── /pendulum.py
    ├── /rendering.py
/simulations
    ├── /double_pendulum
    |   ├── /plots
    |   |   ├── /winner.gv.svg
    |   ├── /__main__.py
    |   ├── /config.json
    |   └── /solutions.txt
    ├── /pendulum
    |   ├── /plots
    |   |    └── /winner.gv.svg
    |   ├── /__main__.py
    |   ├── /config.json
    |   └── /solutions.txt
    ├── /xor
    |   ├── /plots
    |   ├── /winner.gv.svg
    |   ├── /__main__.py
    |   ├── /config.json
    |   └── /solutions.txt
    ├── /activations.py
    ├── /error.py
    ├── /evolution.py
    ├── /gene.py
    ├── /genome.py
    ├── /indexer.py
    ├── /neural_net.py
    ├── /phenotype.py
    ├── /population.py
    ├── /species.py
    ├── /tests.py
    ├── /utils.py
    └── /visualize.py
```

### Explicación de directorios y archivos

* `models/`: Directorio en donde viven los modelos para realizar las evaluaciones de desempeño de las distintas aplicaciones del algoritmo.  
* `models/double_pendulum.py`: Módulo que incluye la clase, métodos y propiedades del péndulo doble invertido doble.
* `models/pendulum.p`: Módulo que incluye la clase del modelo del péndulo invertido sencillo sobre un carro para realizar las simulaciones y animaciones necesarias para los experimentos
* `models/rendering.py`: Módulo necesario para visualizar las animaciones del péndulo invertido sencillo y doble
* `simulations/`: Directorio en donde viven las simulaciones de las diferentes implementaciones de NEAT
* `simulations/<name>/__main__.py`: 
* `simulations/<name>/config.josn`:  
* `simulations/<name>/plots/`: 


## Entidades

El paquete de NeuroPy está conformado por una serie de módulos y clases que interaccionan entre sí para correr el algoritmo, el diagrama de entidades

* Cap 3 Diseño de Neuropy
* Visión general
* Requerimientos
* Tecnología
* Proceso principal del sistema
* Entidades
* Paquetes
* Arquitectura





# Bibliografía

- Modern Control Systems - Ogata





























